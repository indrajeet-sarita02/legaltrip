<?php

return array(
    'modules' => array(
        'Application',
        'User',
        'Admin',
    ),
    'module_listener_options' => array(
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module',
            './vendor',
        ),
    ), 'service_manager' => array(
        'factories' => array(
            'ZendCacheStorageFactory' => function() {
                return \Zend\Cache\StorageFactory::factory(
                                array(
                                    'adapter' => array(
                                        'name' => 'filesystem',
                                        'options' => array(
                                            'clear_stat_cache' => 1,
                                            'dirLevel' => 2,
                                            'cacheDir' => 'data/cache',
                                            'dirPermission' => 0755,
                                            'filePermission' => 0666,
                                            'namespaceSeparator' => '-db-'
                                        ),
                                    ),
                                    'plugins' => array('serializer'),
                                )
                );
            }
                ),
                'aliases' => array(
                    'cache' => 'ZendCacheStorageFactory',
                ),
            ),
        );
        
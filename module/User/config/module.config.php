<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\Index' => 'User\Controller\IndexController',
            'User\Controller\Signup' => 'User\Controller\SignupController',
            'User\Controller\Login' => 'User\Controller\LoginController',
            'User\Controller\Search' => 'User\Controller\SearchController',
            'User\Controller\User' => 'User\Controller\UserController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/user',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.           
                    'login' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/login[/:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Login',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'signup' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/signup[/:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\Signup',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'user' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/user[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'User\Controller\User',
                                'action' => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'layout/layout' => BASE_DIR . '/view/layout/default-layout.phtml',
            'layout/myaccount' => BASE_DIR . '/view/layout/myaccount-layout.phtml',
            'error/404' => BASE_DIR . '/view/error/404.phtml',
            'error/index' => BASE_DIR . '/view/error/index.phtml',
        ),
    ),
    // MODULE CONFIGURATIONS
    'module_config' => array(
        'upload_location' => BASE_DIR . '/uploads/documents',
        'image_upload_location' => BASE_DIR . '/uploads/images'
    ),
    'speck-paypal-api' => array(
        'username' => '<USERNAME>',
        'password' => '<PASSWORD>',
        'signature' => '<SIGNATURE>',
        'endpoint' => 'https://api-3t.sandbox.paypal.com/nvp'
    )
);

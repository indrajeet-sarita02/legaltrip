<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
//  module/User/Module.php

namespace User;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Education;
use User\Model\EducationTable;
use User\Model\Experience;
use User\Model\ExperienceTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Zend\Authentication\AuthenticationService;

class Module implements AutoloaderProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap($e) {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sharedEventManager = $eventManager->getSharedManager(); // The shared event manager

        $sharedEventManager->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, function($e) {
            $controller = $e->getTarget(); // The controller which is dispatched
            $controllerName = $controller->getEvent()->getRouteMatch()->getParam('controller');
            $actionName = $controller->getEvent()->getRouteMatch()->getParam('action');
            $arrController = array('User\Controller\User', 'User\Controller\Index', 'User\Controller\Signup', 'User\Controller\Login');
            if (in_array($controllerName, $arrController) || $actionName == 'detail') {
                $controller->layout('layout/myaccount');
            }
        });
    }

    public function getServiceConfig() {
        return array(
            'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(
                // SERVICES
                'AuthService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'user', 'email', 'password', 'MD5(?)');

                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    return $authService;
                },
                // DB
                'UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                'EducationTable' => function($sm) {
                    $tableGateway = $sm->get('EducationTableGateway');
                    $table = new EducationTable($tableGateway);
                    return $table;
                },
                'EducationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Education());
                    return new TableGateway('education', $dbAdapter, null, $resultSetPrototype);
                },
                'ExperienceTable' => function($sm) {
                    $tableGateway = $sm->get('ExperienceTableGateway');
                    $table = new ExperienceTable($tableGateway);
                    return $table;
                },
                'ExperienceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Experience());
                    return new TableGateway('experience', $dbAdapter, null, $resultSetPrototype);
                },
                // FORMS
                'LoginForm' => function ($sm) {
                    $form = new \User\Form\LoginForm();
                    $form->setInputFilter($sm->get('LoginFilter'));
                    return $form;
                },
                'SignupForm' => function ($sm) {
                    $form = new \User\Form\SignupForm();
                    $form->setInputFilter($sm->get('SignupFilter'));
                    return $form;
                },
                'UserEditForm' => function ($sm) {
                    $form = new \User\Form\UserEditForm();
                    $form->setInputFilter($sm->get('UserEditFilter'));
                    return $form;
                },
                'EducationForm' => function ($sm) {
                    $form = new \User\Form\EducationForm();
                    $form->setInputFilter($sm->get('EducationFilter'));
                    return $form;
                },
                'ExperienceForm' => function ($sm) {
                    $form = new \User\Form\ExperienceForm();
                    $form->setInputFilter($sm->get('ExperienceFilter'));
                    return $form;
                },
                // FILTERS
                'LoginFilter' => function ($sm) {
                    return new \User\Form\LoginFilter();
                },
                'SignupFilter' => function ($sm) {
                    return new \User\Form\SignupFilter();
                },
                'UserEditFilter' => function ($sm) {
                    return new \User\Form\UserEditFilter();
                },
                'EducationFilter' => function ($sm) {
                    return new \User\Form\EducationFilter();
                },
                'ExperienceFilter' => function ($sm) {
                    return new \User\Form\ExperienceFilter();
                },
            ),
            'invokables' => array(),
            'services' => array(),
            'shared' => array(),
        );
    }

}

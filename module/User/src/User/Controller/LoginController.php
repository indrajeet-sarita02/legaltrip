<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Form\LoginForm;
use User\Form\LoginFilter;
use User\Model\User;
use User\Model\UserTable;
use Zend\Facebook\Facebook;

class LoginController extends AbstractActionController {

    protected $storage;
    protected $authservice;

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }

        return $this->authservice;
    }

    public function logoutAction() {
        $this->getAuthService()->clearIdentity();
        $userSession = new \Zend\Session\Container('siteUser');
        $userSession->getManager()->getStorage()->clear('siteUser');
//        $appId = '815960145183239'; //Facebook App ID
//        $appSecret = '6d973751b6d977a6a6f3b17561f13199'; // Facebook App Secret
        //Call Facebook API
        $facebook = new \Zend\Facebook\Facebook(array(
            'appId' => FB_APPID,
            'secret' => FB_APPSECRET
        ));
        $facebook->destroySession();
        unset($userSession->accessToken);
        return $this->redirect()->toRoute('user/login');
    }

    public function indexAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(14);
        $form = $this->getServiceLocator()->get('LoginForm');
        $viewModel = new ViewModel(array('form' => $form, 'content' => $content));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $viewModel = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form, 'content' => $content));
            } else {
                //check authentication...
                $this->getAuthService()->getAdapter()
                        ->setIdentity($post['email'])
                        ->setCredential($post['password']);
                $result = $this->getAuthService()->authenticate();
                if ($result->isValid()) {
                    $this->getAuthService()->getStorage()->write($post['email']);
                    $userTable = $this->getServiceLocator()->get('UserTable');
                    $userData = $userTable->getUserByEmail($post['email']); //pr($user);exit;
                    $userSession = new \Zend\Session\Container('siteUser');
                    $uid = $userSession->id = $userData->id;
                    $userSession->fname = $userData->fname;
                    $userSession->lname = $userData->lname;
                    $userSession->email = $userData->email;
                    $userSession->mobile = $userData->mobile;
                    $userSession->usertype = $userData->usertype;
                    $userSession->status = $userData->status;
                    return $this->redirect()->toRoute('user/user', array('action' => 'index'));
                } else {
                    $viewModel = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form, 'content' => $content));
                }
            }
        }
        return $viewModel;
    }

}

<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\SignupForm;
use User\Form\SignupFilter;
use User\Model\User;

class SignupController extends AbstractActionController {

    public function indexAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(15);
        $form = $this->getServiceLocator()->get('SignupForm');
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();
                $userTable = $this->getServiceLocator()->get('UserTable');
                $userEmail = $data['email'];
                $checkData = $userTable->getUserByEmail($userEmail);

                if (isset($checkData->email) && !empty($checkData->email)) {
                    $model = new ViewModel(array('error' => 'User already Exists.', 'form' => $form, 'content' => $content));
                    return $model;
                } else {
                    // Create user
                    $this->createUser($form->getData());
                    return $this->redirect()->toRoute('user/signup', array('action' => 'confirm'));
                }
            } else {
                $model = new ViewModel(array('error' => 'Something going wrong. Please check again!', 'form' => $form, 'content' => $content));
                return $model;
            }
        }
        $model = new ViewModel(array('form' => $form, 'content' => $content));
        return $model;
    }

    public function confirmAction() {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    protected function createUser(array $data) {
        $user = new User();
        $user->exchangeArray($data);
        $user->setPassword($data['password']);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userTable->saveUser($user);
        return true;
    }

}

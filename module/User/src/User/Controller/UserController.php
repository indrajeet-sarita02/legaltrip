<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Education;
use User\Model\EducationTable;
use User\Model\Experience;
use User\Model\ExperienceTable;

class UserController extends AbstractActionController {

    public function indexAction() {
        $this->layout('layout/myaccount');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $userData = $userTable->getUser($uid);
        $eduData = $this->getServiceLocator()->get('EducationTable')->fetchAll($uid);
        $expData = $this->getServiceLocator()->get('ExperienceTable')->fetchAll($uid);
        $author = $userSession->fname . ' ' . $userSession->lname;
        $userBlog = $this->getServiceLocator()->get('BlogTable')->userBlog($author);

        $viewModel = new ViewModel(array('userData' => $userData, 'eduData' => $eduData, 'expData' => $expData, 'userBlog'=>$userBlog));
        return $viewModel;
    }

    public function editprofileAction() {
        $mstCountryRows = $this->getServiceLocator()->get('UserTable')->getMstCountry();
        $countryData = array();
        foreach ($mstCountryRows as $val) {
            $countryData[$val['name']] = $val['name'];
        }
        $dataParams['countryData'] = $countryData;

        $mstStateRows = $this->getServiceLocator()->get('UserTable')->getMstState();
        $stateData = array();
        foreach ($mstStateRows as $val) {
            $stateData[$val['name']] = $val['name'];
        }
        $dataParams['stateData'] = $stateData;
        $mstCityRows = $this->getServiceLocator()->get('UserTable')->getMstCity();
        $cityData = array();
        foreach ($mstCityRows as $val) {
            $cityData[$val['name']] = $val['name'];
        }
        $dataParams['cityData'] = $cityData;
        $form = new \User\Form\UserEditForm($dataParams); //$this->getServiceLocator()->get('UserEditForm');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        $usertype = $userSession->usertype;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $user = $userTable->getUser($uid);
        $form->bind($user);
        $image = isset($user->image) ? $user->image : '';
        $model = new ViewModel(array('form' => $form, 'user_id' => $uid, 'image' => $image));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $uploadFile = $this->params()->fromFiles('image');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/images";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $imageExists = $uploadPath . "/" . $user->image;
                if (!empty($user->image) && file_exists($imageExists)) {
                    @unlink($imageExists);
                }
                $post['image'] = $newName;
            }
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form, 'image' => $image));
            }
            // update user
            $user = new User();
            $user->exchangeArray($post);
            $this->getServiceLocator()->get('UserTable')->saveUser($user);
            //if ($usertype == 'lawyer') {
            return $this->redirect()->toRoute('user/user', array('action' => 'professionalskill', 'msg' => 1));
            // }
            // return $this->redirect()->toRoute('user/user', array('action' => 'index', 'msg' => 1));
        }

        return $model;
    }

    public function professionalskillAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $userTable = $this->getServiceLocator()->get('UserTable');
        $dataParams = array();
        // master case category data
        $mstCaseCatRows = $userTable->getMstCaseCategory();
        $caseCatData = array();
        foreach ($mstCaseCatRows as $val) {
            $caseCatData[$val['id']] = $val['title'];
        }
        $dataParams['mstCaseCat'] = $caseCatData;

        // master case court data
        $mstCaseCourtRows = $userTable->getMstCaseCourt();
        $caseCourtData = array();
        foreach ($mstCaseCourtRows as $val) {
            $caseCourtData[$val['id']] = $val['title'];
        }
        $dataParams['mstCaseCourt'] = $caseCourtData;

        // master case skill data
        $mstSkillRows = $userTable->getMstSkill();
        $skillData = array();
        foreach ($mstSkillRows as $val) {
            $skillData[$val['id']] = $val['title'];
        }
        $dataParams['mstSkill'] = $skillData;

        $form = new \User\Form\ProfessionalSkillForm($dataParams);
        $model = new ViewModel(array('form' => $form,));

        if (!empty($uid)) {
            $setData = array();
            $selectLawyerCategory = $userTable->selectLawyerCategory($uid);
            $lawyerCatData = array();
            foreach ($selectLawyerCategory as $val) {
                $lawyerCatData[$val['category_id']] = $val['category_id'];
            }
            $setData['category_id'] = $lawyerCatData;
            $selectCaseCourt = $userTable->selectLawyerCourt($uid);
            $lawyerCourtData = array();
            foreach ($selectCaseCourt as $val) {
                $lawyerCourtData[$val['court_id']] = $val['court_id'];
            }
            $setData['court_id'] = $lawyerCourtData;
            $selectSkill = $userTable->selectLawyerSkill($uid);
            $lawyerSkillData = array();
            foreach ($selectSkill as $val) {
                $lawyerSkillData[$val['skill_id']] = $val['skill_id'];
            }
            $setData['skill_id'] = $lawyerSkillData;

            $user = $userTable->getUser($uid);
            if (isset($user->id) && !empty(isset($user->id))) {
                $form->bind($user);
            }

            $model = new ViewModel(array('form' => $form, 'setData' => $setData));
        }

        if ($this->request->isPost()) {
            $saveData = array();
            $post = $this->request->getPost();
            $uploadFile = $this->params()->fromFiles('resume');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/resume";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $resumeExists = $uploadPath . "/" . $user->resume;
                if (!empty($user->resume) && file_exists($resumeExists)) {
                    @unlink($resumeExists);
                }
                $saveData['resume'] = $newName;
            }
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form,));
            }
            $saveData['id'] = $post['id'];
            if (!empty($post['websiteurl'])) {
                $saveData['websiteurl'] = $post['websiteurl'];
            }
            if (!empty($post['summary'])) {
                $saveData['summary'] = $post['summary'];
            }
            // Save Lawyer Category
            $userTable->deleteCaseCategory($uid);
            $userTable->saveCaseCategory($post['category_id'], $uid);
            // Save Lawyer Court
            $userTable->deleteCaseCourt($uid);
            $userTable->saveCaseCourt($post['court_id'], $uid);
            // Save Lawyer Skill
            $userTable->deleteSkill($uid);
            $userTable->saveSkill($post['skill_id'], $uid);
            // update user
            if (count($saveData) > 1) {
                $user = new User();
                $user->exchangeArray($saveData);
                $this->getServiceLocator()->get('UserTable')->saveUser($user);
            }
            return $this->redirect()->toRoute('user/user', array('action' => 'index', 'msg' => 1));
        }
        return $model;
    }

    public function educationAction() {
        $form = $this->getServiceLocator()->get('EducationForm');
        $model = new ViewModel(array('form' => $form,));
        $educationTable = $this->getServiceLocator()->get('EducationTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $id = $this->params()->fromRoute('id');
        if (!empty($id)) {
            $educationData = $educationTable->getEducation($id);
            if ($educationData) {
                $form->bind($educationData);
                $model = new ViewModel(array('form' => $form, 'id' => $id));
            }
        }
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form,));
            }
            // Save education
            $post['user_id'] = $uid;
            $education = new Education();
            $education->exchangeArray($post);
            $this->getServiceLocator()->get('EducationTable')->saveEducation($education);
            return $this->redirect()->toRoute('user/user', array('action' => 'index', 'msg' => 1));
        }
        return $model;
    }

    public function experienceAction() {
        $form = $this->getServiceLocator()->get('ExperienceForm');
        $model = new ViewModel(array('form' => $form,));
        $experienceTable = $this->getServiceLocator()->get('ExperienceTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $id = $this->params()->fromRoute('id');
        if (!empty($id)) {
            $experienceData = $experienceTable->getExperience($id);
            if ($experienceData) {
                $form->bind($experienceData);
                $model = new ViewModel(array('form' => $form, 'id' => $id));
            }
        }
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form,));
            }
            // Save experience
            $post['user_id'] = $uid;
            $experience = new Experience();
            $experience->exchangeArray($post);
            $this->getServiceLocator()->get('ExperienceTable')->saveExperience($experience);
            return $this->redirect()->toRoute('user/user', array('action' => 'index', 'msg' => 1));
        }
        return $model;
    }

    public function deleduAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $this->getServiceLocator()->get('EducationTable')->deleteEducation($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('user/user');
    }

    public function delexpAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $this->getServiceLocator()->get('ExperienceTable')->deleteExperience($this->params()->fromRoute('id'));
        return $this->redirect()->toRoute('user/user');
    }

    public function uploadImageAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $user = $userTable->getUser($uid);
        $staus = false;
        $msg = '';
        if ($this->request->isPost()) {
            $post = array();
            $uploadFile = $this->params()->fromFiles('uploadImage');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/gallery";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $arrImg = explode(",", $user->galleryimage);
                array_push($arrImg, $newName);
                $galleryimage = array_unique($arrImg);
                $post['galleryimage'] = trim(implode(',', $galleryimage), ",");
                chmod($uploadPath . "/" . $newName, 0777);
                $this->getServiceLocator()->get('UserTable')->updateGallery($post, $uid);
                $staus = true;
                $msg = $newName;
            }
        }
        echo json_encode(array('status' => $staus, 'msg' => $msg));
        exit();
    }

    public function deleteImageAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $user = $userTable->getUser($uid);
        $staus = false;
        $msg = '';
        $img = $this->params()->fromQuery('img');
        if ($img) {
            $post = array();
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/gallery/" . $img;
            if (file_exists($uploadPath)) {
                @unlink($uploadPath);
            }
            $user->galleryimage = str_replace($img, '', $user->galleryimage);
            $arrImg = explode(",", $user->galleryimage);
            $galleryimage = array_filter($arrImg);
            $post['galleryimage'] = trim(implode(',', $galleryimage), ",");
            $this->getServiceLocator()->get('UserTable')->updateGallery($post, $uid);
            $staus = true;
            $msg = str_replace('.', '', $img);
        }
        echo json_encode(array('status' => $staus, 'msg' => $msg));
        exit();
    }

    public function updatePasswordAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $msg = '';
        $staus = false;
        //$old_password = $this->params()->fromQuery('old_password');
        $password = $this->params()->fromQuery('password');
        if ($password) {
            $post = array();
            //$post['old_password'] = md5($old_password);
            $post['password'] = md5($password);
            $staus = $this->getServiceLocator()->get('UserTable')->updatePassword($post, $uid);
            if ($staus) {
                $staus = true;
            }
            $msg = 'Your password has been successfully changed.';
        }
        echo json_encode(array('status' => $staus, 'msg' => $msg));
        exit();
    }

}

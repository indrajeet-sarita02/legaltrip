<?php

namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class EducationTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveEducation(Education $row) {
        $data = array(
            'title' => $row->title,
            'description' => $row->description,
            'start_year' => $row->start_year,
            'finish_year' => $row->finish_year,
            'percentage' => $row->percentage,
            'education_type' => $row->education_type,
            'user_id' => $row->user_id,
        );

        $id = (int) $row->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getEducation($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('ID does not exist');
            }
        }
    }

    /**
     * Get all education
     * @return ResultSet
     */
    public function fetchAll($uid) {
        $resultSet = $this->tableGateway->select(array('user_id' => $uid));
        return $resultSet;
    }

    /**
     * Get education by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getEducation($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Delete education by Id
     * @param string $id
     */
    public function deleteEducation($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}

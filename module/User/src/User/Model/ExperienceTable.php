<?php

namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ExperienceTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveExperience(Experience $exp) {
        $data = array(
            'company' => $exp->company,
            'designation' => $exp->designation,
            'join_date' => $exp->join_date,
            'release_date' => $exp->release_date,
            'user_id' => $exp->user_id,
        );

        $id = (int) $exp->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getExperience($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('ID does not exist');
            }
        }
    }

    /**
     * Get all education
     * @return ResultSet
     */
    public function fetchAll($uid) {
        $resultSet = $this->tableGateway->select(array('user_id' => $uid));
        return $resultSet;
    }

    /**
     * Get education by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getExperience($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Delete education by Id
     * @param string $id
     */
    public function deleteExperience($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}

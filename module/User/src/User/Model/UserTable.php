<?php

namespace User\Model;

use Zend\Text\Table\Row;
#use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;

class UserTable {

    protected $tableGateway;

    /**
     * default construct
     * @return ResultSet
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Get all user
     * @return ResultSet
     */
    public function fetchAll($queryText = null) {
        // create a new Select object for the table user
        $select = new Select('user');
        if ($queryText) {
            // Create array containing the fields and their expected values
            $conditions = array('fname' => $queryText, 'lname' => $queryText, 'email' => $queryText);
            // Add these fields to the WHERE clause of the query but place "OR" in between
            $select->where($conditions, \Zend\Db\Sql\Predicate\PredicateSet::OP_OR);
            $select = $select->where($conditions);
        }
        // create a new result set based on the User entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new User());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    /**
     * save and update user
     * @return ResultSet
     */
    public function saveUser(User $user) {
        $data = array();

        if (isset($user->fname) && !empty($user->fname)) {
            $data['fname'] = $user->fname;
        }
        if (isset($user->lname) && !empty($user->lname)) {
            $data['lname'] = $user->lname;
        }
        if (isset($user->image) && !empty($user->image)) {
            $data['image'] = $user->image;
        }
        if (isset($user->email) && !empty($user->email)) {
            $data['email'] = $user->email;
        }
        if (isset($user->usertype) && !empty($user->usertype)) {
            $data['usertype'] = $user->usertype;
        }
        if (isset($user->dob) && !empty($user->dob)) {
            $data['dob'] = $user->dob;
        }
        if (isset($user->aboutus) && !empty($user->aboutus)) {
            $data['aboutus'] = $user->aboutus;
        }
        if (isset($user->otherinfo) && !empty($user->otherinfo)) {
            $data['otherinfo'] = $user->otherinfo;
        }
        if (isset($user->resume) && !empty($user->resume)) {
            $data['resume'] = $user->resume;
        }
        if (isset($user->address) && !empty($user->address)) {
            $data['address'] = $user->address;
        }
        if (isset($user->address2) && !empty($user->address2)) {
            $data['address2'] = $user->address2;
        }
        if (isset($user->city) && !empty($user->city)) {
            $data['city'] = $user->city;
        }
        if (isset($user->state) && !empty($user->state)) {
            $data['state'] = $user->state;
        }
        if (isset($user->country) && !empty($user->country)) {
            $data['country'] = $user->country;
        }
        if (isset($user->zip) && !empty($user->zip)) {
            $data['zip'] = $user->zip;
        }
        if (isset($user->phone) && !empty($user->phone)) {
            $data['phone'] = $user->phone;
        }
        if (isset($user->mobile) && !empty($user->mobile)) {
            $data['mobile'] = $user->mobile;
        }
        if (isset($user->fax) && !empty($user->fax)) {
            $data['fax'] = $user->fax;
        }
        if (isset($user->email1) && !empty($user->email1)) {
            $data['email1'] = $user->email1;
        }
        if (isset($user->summary) && !empty($user->summary)) {
            $data['summary'] = $user->summary;
        }
        if (isset($user->websiteurl) && !empty($user->websiteurl)) {
            $data['websiteurl'] = $user->websiteurl;
        }
        if (isset($user->isblock) && !empty($user->isblock)) {
            $data['isblock'] = $user->isblock;
        }
        if (isset($user->status) && !empty($user->status)) {
            $data['status'] = $user->status;
        }

        if (isset($user->password) && !empty($user->password)) {
            $data['password'] = $user->password;
        }
        if (isset($user->registerdate) && !empty($user->registerdate)) {
            $data['registerdate'] = $user->registerdate;
        }
        if (isset($user->lastupdated) && !empty($user->lastupdated)) {
            $data['lastupdated'] = $user->lastupdated;
        }
        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                if (empty($data['password'])) {
                    unset($data['password']);
                }
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User ID does not exist');
            }
        }
    }

    /**
     * update Gallery images
     * @return ResultSet
     */
    public function updateGallery($data, $id) {
        $resultSet = $this->tableGateway->update($data, array('id' => $id));
        return $resultSet;
    }

    /**
     * update Update password
     * @return true/false
     */
    public function updatePassword($data, $id) {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (isset($row->id) && !empty($row->id)) {
            $this->tableGateway->update($data, array('id' => $id));
            return true;
        }
        return false;
    }

    /**
     * Get all user
     * @return ResultSet
     */
    public function fetchData($where = array(), $limit = 5) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('sd' => 'search_data'));
        $select->where($where);
        $select->limit($limit);
        $query = $sql->prepareStatementForSqlObject($select);
        //print($query->getSql());exit;
        $resutl = $query->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($resutl);
        return $resultSet;
    }

    /**
     * Get search Data user detail
     * @throws \Exception
     * @return Rows
     */
    public function searchData($id) {
        $adapter = $this->tableGateway->getAdapter();
        $searchTable = new TableGateway('search_data', $adapter);
        $searchRows = $searchTable->select(array('id' => $id));
        $row = $searchRows->current();
        return $row;
    }

    /**
     * Get User account by UserId
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getUser($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get User account by Email
     * @param string $userEmail
     * @throws \Exception
     * @return Row
     */
    public function getUserByEmail($userEmail) {
        $rowset = $this->tableGateway->select(array('email' => $userEmail));
        $row = $rowset->current();
        if (!$row) {
            return false;
//            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }

    /**
     * Delete User account by UserId
     * @param string $id
     */
    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

    /**
     * Get Mst Case Category
     * @throws \Exception
     * @return Rows
     */
    public function getMstCaseCategory() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_case_category'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.title ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstCaseCatRows = $statement->execute();
        return $mstCaseCatRows;
    }

    /**
     * Get Mst Case Court
     * @throws \Exception
     * @return Rows
     */
    public function getMstCaseCourt() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_case_court'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.title ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstCaseCourtRows = $statement->execute();
        return $mstCaseCourtRows;
    }

    /**
     * Get Mst Skill
     * @throws \Exception
     * @return Rows
     */
    public function getMstSkill() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_skill'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.title ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstSkillRows = $statement->execute();
        return $mstSkillRows;
    }

    /**
     * Get Mst Location
     * @throws \Exception
     * @return Rows
     */
    public function getMstCountry() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_country'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.name ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstCountryRows = $statement->execute();
        return $mstCountryRows;
    }

    /**
     * Get Mst Location
     * @throws \Exception
     * @return Rows
     */
    public function getMstState() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_state'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.name ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstStateRows = $statement->execute();
        return $mstStateRows;
    }

    /**
     * Get Mst City
     * @throws \Exception
     * @return Rows
     */
    public function getMstCity() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_city'));
        $select->where(array('ls.status' => 1));
        $select->order(array('ls.name ASC'));
        $select->limit(1000);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $mstCityRows = $statement->execute();
        return $mstCityRows;
    }

    /**
     * Get Mst Case Category
     * @throws \Exception
     * @return Rows
     */
    public function saveCaseCategory($param, $uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCat = new TableGateway('lawyer_category', $adapter);
        foreach ($param as $val) {
            $data = array(
                'lawyer_id' => $uid,
                'category_id' => $val);
            $mstCaseCat->insert($data);
        }
    }

    /**
     * Get Mst Case Court
     * @throws \Exception
     * @return Rows
     */
    public function saveCaseCourt($param, $uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCourt = new TableGateway('lawyer_court', $adapter);
        foreach ($param as $val) {
            $data = array(
                'lawyer_id' => $uid,
                'court_id' => $val);
            $mstCaseCourt->insert($data);
        }
    }

    /**
     * Get Mst Skill
     * @throws \Exception
     * @return Rows
     */
    public function saveSkill($param, $uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstSkill = new TableGateway('lawyer_skill', $adapter);
        foreach ($param as $val) {
            $data = array(
                'lawyer_id' => $uid,
                'skill_id' => $val);
            $mstSkill->insert($data);
        }
    }

    /**
     * Delete lawyer category by UserId
     * @param string $uid
     */
    public function deleteCaseCategory($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCat = new TableGateway('lawyer_category', $adapter);
        $mstCaseCat->delete(array('lawyer_id' => $uid));
    }

    /**
     * Delete lawyer court by UserId
     * @param string $uid
     */
    public function deleteCaseCourt($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCourt = new TableGateway('lawyer_court', $adapter);
        $mstCaseCourt->delete(array('lawyer_id' => $uid));
    }

    /**
     * Delete lawyer Skill by UserId
     * @param string $uid
     */
    public function deleteSkill($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstSkill = new TableGateway('lawyer_skill', $adapter);
        $mstSkill->delete(array('lawyer_id' => $uid));
    }

    /**
     * select lawyer category by UserId
     * @param string $uid
     */
    public function selectLawyerCategory($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCat = new TableGateway('lawyer_category', $adapter);
        $rows = $mstCaseCat->select(array('lawyer_id' => $uid));
        return $rows;
    }

    /**
     * select lawyer court by UserId
     * @param string $uid
     */
    public function selectLawyerCourt($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstCaseCourt = new TableGateway('lawyer_court', $adapter);
        $rows = $mstCaseCourt->select(array('lawyer_id' => $uid));
        return $rows;
    }

    /**
     * select lawyer Skill by UserId
     * @param string $uid
     */
    public function selectLawyerSkill($uid) {
        $adapter = $this->tableGateway->getAdapter();
        $mstSkill = new TableGateway('lawyer_skill', $adapter);
        $rows = $mstSkill->select(array('lawyer_id' => $uid));
        return $rows;
    }

    public function getLawyerCategory($uid) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('lc' => 'lawyer_category'));
        $select->join(array('mcat' => 'mst_case_category'), 'mcat.id = lc.category_id', array('title'), $select::JOIN_INNER);
        $select->where(array('lc.lawyer_id' => $uid));
        $select->group(array('mcat.title'));
        $select->order(array('mcat.id ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $rec = $statement->execute();
        return $rec;
    }

    public function getLawyerCourt($uid) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('lc' => 'lawyer_court'));
        $select->join(array('mcas' => 'mst_case_court'), 'mcas.id = lc.court_id', array('title'), $select::JOIN_INNER);
        $select->where(array('lc.lawyer_id' => $uid));
        $select->group(array('mcas.title'));
        $select->order(array('mcas.id ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $rec = $statement->execute();
        return $rec;
    }

    public function getLawyerSkill($uid) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'lawyer_skill'));
        $select->join(array('msk' => 'mst_skill'), 'msk.id = ls.skill_id', array('title'), $select::JOIN_INNER);
        $select->where(array('ls.lawyer_id' => $uid));
        $select->group(array('msk.title'));
        $select->order(array('msk.id ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $rec = $statement->execute();
        return $rec;
    }

    public function getAllUsers($limit, $page) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'user'));
        $select->where(array('ls.status' => array(0, 1)));
        $select->limit($limit);
        $select->offset($page);
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());echo "\n";
        $rec = $statement->execute();
        return $rec;
    }

    public function getCount() {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'user'));
        $select->columns(array('cnt' => new \Zend\Db\Sql\Expression("COUNT(ls.id)")));
        $select->where(array('ls.status' => array(0, 1)));
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $rec = $statement->execute();
        $row = $rec->current();
        return $row;
    }

    public function getCityByState($name) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select()->from(array('ls' => 'mst_city'));
        $select->join(array('ms' => 'mst_state'), 'ms.id = ls.state_id', array(), $select::JOIN_INNER);
        $select->where(array('ms.name' => $name));
        $select->order(array('ls.name ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        //print($statement->getSql());
        $rec = $statement->execute();
        return $rec;
    }

    public function getLatLng($address) {
        $address = urlencode(trim($address));
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $address . "&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);
        // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
        if ($response['status'] != 'OK') {
            return null;
        }
        //print_r($response);
        //print_r($response['results'][0]['geometry']['location']);
        $latLng = $response['results'][0]['geometry']['location'];
        return $latLng;
    }

    public function siteMapXML($id, $name, $status) {
        $siteMap = BASE_DIR . '/sitemap.xml';
        if ($status == 0) {
            @unlink($siteMap);
            file_put_contents($siteMap, '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.google.com/schemas/sitemap/0.90">', FILE_APPEND);
        }
        if ($status == 1) {
            $url = BASE_URL . "/search/detail/" . $id . "_" . $this->cleanUrl($name);
            $xml = '<url><loc>' . $url . '</loc>
                            <lastmod>' . date("Y-m-d") . '</lastmod>
                            <changefreq>weekly</changefreq>
                            <priority>0.5</priority>
                          </url>';
            file_put_contents($siteMap, $xml, FILE_APPEND);
        }
        if ($status == 2) {
            file_put_contents($siteMap, '</urlset>', FILE_APPEND);
        }
    }

    public function cleanUrl($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return trim(strtolower(preg_replace('/-+/', '-', $string)), '-') . ".html"; // Replaces multiple hyphens with single one.
    }

}

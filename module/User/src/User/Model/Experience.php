<?php

namespace User\Model;

class Experience {

    public $id;
    public $company;
    public $designation;
    public $join_date;
    public $release_date;
    public $user_id;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->company = (isset($data['company'])) ? $data['company'] : '';
        $this->designation = (isset($data['designation'])) ? $data['designation'] : '';
        $this->join_date = (isset($data['join_date'])) ? $data['join_date'] : '';
        $this->release_date = (isset($data['release_date'])) ? $data['release_date'] : '';
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

<?php

namespace User\Model;

class Education {

    public $id;
    public $title;
    public $description;
    public $start_year;
    public $finish_year;
    public $percentage;
    public $education_type;
    public $user_id;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->title = (isset($data['title'])) ? $data['title'] : '';
        $this->description = (isset($data['description'])) ? $data['description'] : '';
        $this->start_year = (isset($data['start_year'])) ? $data['start_year'] : '';
        $this->finish_year = (isset($data['finish_year'])) ? $data['finish_year'] : '';
        $this->percentage = (isset($data['percentage'])) ? $data['percentage'] : '';
        $this->education_type = (isset($data['education_type'])) ? $data['education_type'] : '';
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

<?php

namespace User\Form;

use Zend\Form\Form;

class EducationForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Education');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
                'id' => 'description',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        $year = $this->getYear();
        
        $this->add(array(
            'name' => 'start_year',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'start_year',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Start year',
                'empty_option' => 'Select',
                'value_options' => $year,
            ),
        ));
        $this->add(array(
            'name' => 'finish_year',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'finish_year',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Finish year',
                'empty_option' => 'Select',
                'value_options' => $year,
            ),
        ));

        $this->add(array(
            'name' => 'percentage',
            'attributes' => array(
                'type' => 'text',
                'id' => 'percentage',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Percentage',
            ),
        ));
        $this->add(array(
            'name' => 'education_type',
            'attributes' => array(
                'type' => 'text',
                'id' => 'education_type',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Education type',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'value' => 'Submit'
            ),
        ));
    }

    public function getYear() {
        $year = date('Y');
        $x = 1940;
        $data = array();
        for ($x; $x <= $year; $x++) {
            $data[$x] = $x;
        }
        return $data;
    }

}

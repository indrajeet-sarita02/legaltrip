<?php

namespace User\Form;

use Zend\Form\Form;

class UserEditForm extends Form {

    public function __construct($params = null) {
        parent::__construct('Edit User');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'fname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fname',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'First Name',
            ),
        ));

        $this->add(array(
            'name' => 'lname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lname',
                'class' => 'form-control',
                //'required' => 'required'
            ),
            'options' => array(
                'label' => 'Last Name',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'id' => 'email',
                'class' => 'form-control',
                'required' => 'required',
                'readonly' => 'readonly'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        /* $this->add(array(
          'name' => 'password',
          'attributes' => array(
          'type' => 'password',
          'id' => 'password',
          'class' => 'form-control',
          'required' => 'required'
          ),
          'options' => array(
          'label' => 'Password',
          ),
          )); */

        $this->add(array(
            'name' => 'dob',
            'attributes' => array(
                'type' => 'text',
                'id' => 'dob',
                'class' => 'form-control',
                'readonly' => 'readonly',
            ),
            'options' => array(
                'label' => 'Date of birth',
            ),
        ));

        $this->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
                'id' => 'mobile',
                'class' => 'form-control',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Mobile',
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Phone No.',
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address',
                'class' => 'form-control',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));
        $this->add(array(
            'name' => 'address2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address2',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Other Address',
            ),
        ));

        $this->add(array(
            'name' => 'aboutus',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'aboutus',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'About Us',
            ),
        ));

        $this->add(array(
            'name' => 'otherinfo',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'otherinfo',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Other Info',
            ),
        ));

        $this->add(array(
            'name' => 'fax',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fax',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Fax No.',
            ),
        ));

        $this->add(array(
            'name' => 'email1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email1',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Other email',
            ),
        ));

        $this->add(array(
            'name' => 'image',
            'attributes' => array(
                'type' => 'file',
                'id' => 'image',
                'class' => 'form-control-file',
            // 'required' => 'required'
            ),
            'options' => array(
                'label' => 'Image',
            ),
        ));

        $this->add(array(
            'name' => 'country',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'country',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Country',
                'empty_option' => 'Select',
                'value_options' => $params['countryData'],
            ),
        ));

        $this->add(array(
            'name' => 'state',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'state',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'State',
                'empty_option' => 'Select',
                'value_options' => $params['stateData'],
            ),
        ));

        $this->add(array(
            'name' => 'city',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'city',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'City',
                'empty_option' => 'Select',
                'value_options' => $params['cityData'],
            ),
        ));
        
        $this->add(array(
            'name' => 'zip',
            'attributes' => array(
                'type' => 'text',
                'id' => 'zip',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Zipcode',
            ),
        ));

        $this->add(array(
            'name' => 'usertype',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'value' => 'Submit'
            ),
        ));
    }

}

<?php

namespace User\Form;

use Zend\Form\Form;

class ExperienceForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Experience');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Company',
            ),
        ));

        $this->add(array(
            'name' => 'designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'designation',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Designation',
            ),
        ));

        $this->add(array(
            'name' => 'join_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'join_date',
                'readonly' => 'readonly',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Join date',
            ),
        ));
        $this->add(array(
            'name' => 'release_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'release_date',
                'readonly' => 'readonly',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Release date',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'value' => 'Submit'
            ),
        ));
    }

}

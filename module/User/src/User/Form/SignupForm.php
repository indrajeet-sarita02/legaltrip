<?php

namespace User\Form;

use Zend\Form\Form;

class SignupForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Signup');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'fname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fname',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'First Name',
            ),
        ));

        $this->add(array(
            'name' => 'lname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lname',
                'class' => 'form-control',
                //'required' => 'required'
            ),
            'options' => array(
                'label' => 'Last Name',
            ),
        ));


        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'id' => 'email',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));

        $this->add(array(
            'name' => 'confirm_password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'confirm_password',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Confirm Password',
            ),
        ));

        $this->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
                'id' => 'mobile',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Mobile',
            ),
        ));
        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address',
                'class' => 'form-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));
        /* $this->add(array(
          'name' => 'aboutus',
          'attributes' => array(
          'type' => 'text',
          'class' => 'form-control',
          'required' => 'required'
          ),
          'options' => array(
          'label' => 'About Us',
          ),
          ));
          $this->add(array(
          'name' => 'otherinfo',
          'attributes' => array(
          'type' => 'textarea',
          'class' => 'form-control',
          'required' => 'required'
          ),
          'options' => array(
          'label' => 'Other Info',
          ),
          ));
          $this->add(array(
          'name' => 'image',
          'attributes' => array(
          'type' => 'file',
          'class' => 'form-control',
          'required' => 'required'
          ),
          'options' => array(
          'label' => 'Image',
          ),
          )); */
        $this->add(array(
            'name' => 'usertype',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'usertype',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Signup As',
                'empty_option' => 'Select',
                'value_options' => (array) json_decode(USER_TYPE),
            ),
        ));

        $this->add(array(
            'name' => 'registerdate',
            'attributes' => array(
                'type' => 'hidden',
                'value' => time(),
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Submit'
            ),
        ));
    }

}

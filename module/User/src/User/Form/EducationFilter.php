<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;

class EducationFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'start_year',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'finish_year',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'percentage',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'education_type',
            'required' => true,
        ));
    }

}

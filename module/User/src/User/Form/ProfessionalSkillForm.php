<?php

namespace User\Form;

use Zend\Form\Form;

class ProfessionalSkillForm extends Form {

    public function __construct($params = array()) {
        parent::__construct('Professional Skill');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'resume',
            'attributes' => array(
                'type' => 'file',
                'class' => 'form-control',
               // 'required' => 'required'
            ),
            'options' => array(
                'label' => 'Resume',
            ),
        ));
        
        $this->add(array(
            'name' => 'websiteurl',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                //'required' => 'required'
            ),
            'options' => array(
                'label' => 'Website URL',
            ),
        ));
        
        $this->add(array(
            'name' => 'summary',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'form-control',
                //'required' => 'required'
            ),
            'options' => array(
                'label' => 'Summary',
            ),
        ));
        
        $this->add(array(
            'name' => 'category_id',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'category_id',
                'multiple' => 'multiple',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Category',
                'empty_option' => 'Select',
                'value_options' => $params['mstCaseCat'],
            ),
        ));
        $this->add(array(
            'name' => 'court_id',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'court_id',
                'multiple' => 'multiple',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Court',
                'empty_option' => 'Select',
                'value_options' => $params['mstCaseCourt'],
            ),
        ));
        $this->add(array(
            'name' => 'skill_id',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'skill_id',
                'multiple' => 'multiple',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Skill',
                'empty_option' => 'Select',
                'value_options' => $params['mstSkill'],
            ),
        ));
       
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'value' => 'Submit'
            ),
        ));
    }

}

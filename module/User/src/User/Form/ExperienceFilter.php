<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;

class ExperienceFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'company',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'designation',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'join_date',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'join_date',
            'required' => true,
        ));
    }

}

<?php

namespace Admin\Model;

class Content {

    public $id;
    public $title;
    public $content;
    public $meta_title;
    public $meta_keyword;
    public $meta_description;
    public $status;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->title = (isset($data['title'])) ? $data['title'] : '';
        $this->content = (isset($data['content'])) ? $data['content'] : '';
        $this->meta_title = (isset($data['meta_title'])) ? $data['meta_title'] : '';
        $this->meta_keyword = (isset($data['meta_keyword'])) ? $data['meta_keyword'] : '';
        $this->meta_description = (isset($data['meta_description'])) ? $data['meta_description'] : '';
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

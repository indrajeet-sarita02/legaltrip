<?php

namespace Admin\Model;

use Zend\Text\Table\Row;
#use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class BlogTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($query = null, $categoryId = 0) {
        $adapter = $this->tableGateway->getAdapter();
        // create a new Select object for the table user
        $select = new Select('blog');
        $select->join(array('category' => 'category'), 'category.id = blog.category_id', array('catTitle' => 'title'), $select::JOIN_LEFT);
        $select->join(array('comment' => 'comment'), 'blog.id = comment.blog_id', array('cnt' => new \Zend\Db\Sql\Expression("COUNT(comment.content)")), $select::JOIN_LEFT);
        $select->where->in('blog.status', array(0, 1));
        if ($categoryId > 0) {
            $select->where->equalTo('blog.category_id', $categoryId);
        }
        if ($query) {
            $query = trim($query);
            $select->where->AND->nest()->like('blog.title', '%' . $query . '%')->OR->like('blog.content', '%' . $query . '%');
        }
        $select->group(array('blog.id'));
        $select->order(array('blog.id DESC'));
        //echo $select->getSqlString();
        // create a new result set based on the Blog entity
        $resultSetPrototype = new ResultSet();
        //$resultSetPrototype->setArrayObjectPrototype(new Blog());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $adapter,
                // the result set to hydrate
                $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function saveBlog($data) {
        $saveData = array();
        if (isset($data->title) && !empty($data->title)) {
            $saveData['title'] = $data->title;
        }

        if (isset($data->category_id) && !empty($data->category_id)) {
            $saveData['category_id'] = $data->category_id;
        }

        if (isset($data->content) && !empty($data->content)) {
            $saveData['content'] = $data->content;
        }

        if (isset($data->status)) {
            $saveData['status'] = $data->status;
        }

        if (isset($data->author)) {
            $saveData['author'] = $data->author;
        }

        $id = (int) $data->id;
        if ($id == 0) {
            $saveData['created'] = time();
            $this->tableGateway->insert($saveData);
        } else {
            if ($this->getBlog($id)) {
                $saveData['updated'] = time();
                $this->tableGateway->update($saveData, array('id' => $id));
            } else {
                throw new \Exception('Blog ID does not exist');
            }
        }
    }

    /**
     * Get Blog by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getBlog($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Blog by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function userBlog($author) {
        $rowset = $this->tableGateway->select(array('author' => $author, 'status' => array(0, 1)));
        if (!$rowset) {
            return;
            throw new \Exception("Could not find row $author");
        }
        return $rowset;
    }

    /**
     * Get content account by title
     * @param string $title
     * @throws \Exception
     * @return Row
     */
    public function getBlogByTitle($title) {
        $rowset = $this->tableGateway->select(array('title' => $title));
        $row = $rowset->current();
        if (!$row) {
            return false;
//            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }

    /**
     * update Update password
     * @return true/false
     */
    public function updateFields($data, $id) {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (isset($row->id) && !empty($row->id)) {
            $this->tableGateway->update($data, array('id' => $id));
            return $row;
        }
        return false;
    }

    /**
     * Delete Blog by Id
     * @param string $id
     */
    public function deleteBlog($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

    /**
     * Get user comments
     * @throws \Exception
     * @return Rows
     */
    public function getComment($id = 0, $limit = 0, $type = '') {
        $select = new Select('comment');
        $data = array();
        if ($id > 0) {
            $data['blog_id'] = $id;
        }
        if ($type == 'admin') {
            $data['status'] = array(0, 1);
        } else {
            $data['status'] = 1;
        }

        $select->where($data);
        $select->order('id DESC');
        if ($limit > 0) {
            $select->limit($limit);
        }
        $resultSet = $this->tableGateway->selectWith($select);
        if (!$resultSet) {
            throw new \Exception("Could not find rows with food id - $id");
        }
        return $resultSet;
    }

    /**
     * Get user Category
     * @throws \Exception
     * @return Rows
     */
    public function getCategory() {
        $select = new Select('category');
        $select->where(array('type' => 'blog', 'status' => 1));
        $select->order('id DESC');
        $resultSet = $this->tableGateway->selectWith($select);
        if (!$resultSet) {
            throw new \Exception("Could not find rows");
        }
        return $resultSet;
    }

}

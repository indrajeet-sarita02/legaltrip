<?php

namespace Admin\Model;

use Zend\Text\Table\Row;
#use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class UserTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($query = null) {
        // create a new Select object for the table user
        $select = new Select('user');
        if ($query) {
            $query = trim($query);
            // Create array containing the fields and their expected values
            if (is_numeric($query)) {
                $conditions = array('phone' => $query, 'mobile' => $query);
            } else {
                $conditions = array('fname' => $query, 'lname' => $query, 'email' => $query);
            }
            // Add these fields to the WHERE clause of the query but place "OR" in between
            $select->where($conditions, \Zend\Db\Sql\Predicate\PredicateSet::OP_OR);
            $select = $select->where($conditions);
        }
        $select->where->in('status', array(0, 1));
        $select->order(array('id DESC'));
        // create a new result set based on the User entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new User());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function saveUser($user) {
        $data = array();
        if (isset($user->fname) && !empty($user->fname)) {
            $data['fname'] = $user->fname;
        }
        if (isset($user->lname) && !empty($user->lname)) {
            $data['lname'] = $user->lname;
        }
        if (isset($user->image) && !empty($user->image)) {
            $data['image'] = $user->image;
        }
        if (isset($user->email) && !empty($user->email)) {
            $data['email'] = $user->email;
        }
        if (isset($user->usertype) && !empty($user->usertype)) {
            $data['usertype'] = $user->usertype;
        }
        if (isset($user->dob) && !empty($user->dob)) {
            $data['dob'] = $user->dob;
        }
        if (isset($user->aboutus) && !empty($user->aboutus)) {
            $data['aboutus'] = $user->aboutus;
        }
        if (isset($user->otherinfo) && !empty($user->otherinfo)) {
            $data['otherinfo'] = $user->otherinfo;
        }
        if (isset($user->resume) && !empty($user->resume)) {
            $data['resume'] = $user->resume;
        }
        if (isset($user->address) && !empty($user->address)) {
            $data['address'] = $user->address;
        }
        if (isset($user->address2) && !empty($user->address2)) {
            $data['address2'] = $user->address2;
        }
        if (isset($user->city) && !empty($user->city)) {
            $data['city'] = $user->city;
        }
        if (isset($user->state) && !empty($user->state)) {
            $data['state'] = $user->state;
        }
        if (isset($user->country) && !empty($user->country)) {
            $data['country'] = $user->country;
        }
        if (isset($user->zip) && !empty($user->zip)) {
            $data['zip'] = $user->zip;
        }
        if (isset($user->phone) && !empty($user->phone)) {
            $data['phone'] = $user->phone;
        }
        if (isset($user->mobile) && !empty($user->mobile)) {
            $data['mobile'] = $user->mobile;
        }
        if (isset($user->fax) && !empty($user->fax)) {
            $data['fax'] = $user->fax;
        }
        if (isset($user->email1) && !empty($user->email1)) {
            $data['email1'] = $user->email1;
        }
        if (isset($user->summary) && !empty($user->summary)) {
            $data['summary'] = $user->summary;
        }
        if (isset($user->websiteurl) && !empty($user->websiteurl)) {
            $data['websiteurl'] = $user->websiteurl;
        }
        if (isset($user->isblock) && !empty($user->isblock)) {
            $data['isblock'] = $user->isblock;
        }
        if (isset($user->status) && !empty($user->status)) {
            $data['status'] = $user->status;
        }

        if (isset($user->password) && !empty($user->password)) {
            $data['password'] = $user->password;
        }
        if (isset($user->registerdate) && !empty($user->registerdate)) {
            $data['registerdate'] = $user->registerdate;
        }
        if (isset($user->lastupdated) && !empty($user->lastupdated)) {
            $data['lastupdated'] = $user->lastupdated;
        }
        $id = (int) $user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getUser($id)) {
                if (empty($data['password'])) {
                    unset($data['password']);
                }
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User ID does not exist');
            }
        }
        return $id;
    }

    /**
     * Get User account by UserId
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getUser($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get User account by Email
     * @param string $userEmail
     * @throws \Exception
     * @return Row
     */
    public function getUserByEmail($userEmail) {
        $rowset = $this->tableGateway->select(array('email' => $userEmail));
        $row = $rowset->current();
        if (!$row) {
            return false;
//            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }

    /**
     * update Update password
     * @return true/false
     */
    public function updateFields($data, $id) {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (isset($row->id) && !empty($row->id)) {
            $this->tableGateway->update($data, array('id' => $id));
            return $row;
        }
        return false;
    }

    /**
     * Delete User account by UserId
     * @param string $id
     */
    public function deleteUser($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}

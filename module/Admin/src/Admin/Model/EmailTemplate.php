<?php

namespace Admin\Model;

class EmailTemplate {

    public $id;
    public $emailto;
    public $emailfrom;
    public $title;
    public $subject;
    public $identifire;
    public $content;
    public $keyword;
    public $status;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->emailto = (isset($data['emailto'])) ? $data['emailto'] : '';
        $this->emailfrom = (isset($data['emailfrom'])) ? $data['emailfrom'] : '';
        $this->title = (isset($data['title'])) ? $data['title'] : '';
        $this->subject = (isset($data['subject'])) ? $data['subject'] : '';
        $this->identifire = (isset($data['identifire'])) ? $data['identifire'] : '';
        $this->content = (isset($data['content'])) ? $data['content'] : '';
        $this->keyword = (isset($data['keyword'])) ? $data['keyword'] : '';
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

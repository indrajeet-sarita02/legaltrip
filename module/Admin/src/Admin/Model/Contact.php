<?php

namespace Admin\Model;

class Contact {

    public $id;
    public $name;
    public $email;
    public $mobile;
    public $subject;
    public $message;
    public $type;
    public $status;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->name = (isset($data['name'])) ? $data['name'] : '';
        $this->email = (isset($data['email'])) ? $data['email'] : '';
        $this->mobile = (isset($data['mobile'])) ? $data['mobile'] : '';
        $this->subject = (isset($data['subject'])) ? $data['subject'] : '';
        $this->message = (isset($data['message'])) ? $data['message'] : '';
        $this->type = (isset($data['type'])) ? $data['type'] : '';
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

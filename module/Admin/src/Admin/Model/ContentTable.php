<?php

namespace Admin\Model;

use Zend\Text\Table\Row;
#use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ContentTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($query = null) {
        // create a new Select object for the table user
        $select = new Select('content');
        if ($query) {
            $query = trim($query);
            // Create array containing the fields and their expected values
            $conditions = array('title' => $query, 'content' => $query);
            // Add these fields to the WHERE clause of the query but place "OR" in between
            $select->where($conditions, \Zend\Db\Sql\Predicate\PredicateSet::OP_OR);
            $select = $select->where($conditions);
        }
        $select->order(array('id DESC'));
        // create a new result set based on the Content entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Content());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $this->tableGateway->getAdapter(),
                // the result set to hydrate
                $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function saveContent($data) {
        $saveData = array();
        if (isset($data->title) && !empty($data->title)) {
            $saveData['title'] = $data->title;
        }
        if (isset($data->content) && !empty($data->content)) {
            $saveData['content'] = $data->content;
        }
        if (isset($data->meta_title) && !empty($data->meta_title)) {
            $saveData['meta_title'] = $data->meta_title;
        }
        if (isset($data->meta_keyword) && !empty($data->meta_keyword)) {
            $saveData['meta_keyword'] = $data->meta_keyword;
        }
        if (isset($data->meta_description) && !empty($data->meta_description)) {
            $saveData['meta_description'] = $data->meta_description;
        }

        if (isset($data->status)) {
            $saveData['status'] = $data->status;
        }

        $id = (int) $data->id;
        if ($id == 0) {
            $saveData['created'] = time();
            $this->tableGateway->insert($saveData);
        } else {
            if ($this->getContent($id)) {
                $saveData['updated'] = time();
                $this->tableGateway->update($saveData, array('id' => $id));
            } else {
                throw new \Exception('Content ID does not exist');
            }
        }
    }

    /**
     * Get Content by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getContent($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id, 'status' => 1));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get content account by title
     * @param string $title
     * @throws \Exception
     * @return Row
     */
    public function getContentByTitle($title) {
        $rowset = $this->tableGateway->select(array('title' => $title));
        $row = $rowset->current();
        if (!$row) {
            return false;
//            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }

    /**
     * update Update password
     * @return true/false
     */
    public function updateFields($data, $id) {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (isset($row->id) && !empty($row->id)) {
            $this->tableGateway->update($data, array('id' => $id));
            return $row;
        }
        return false;
    }

    /**
     * Delete Content by Id
     * @param string $id
     */
    public function deleteContent($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

    public function Leases($poolid) {
        $result = $this->select(function (Select $select) use ($poolid) {
            $select
                    ->columns(array(
                        'ipaddress',
                        'accountid',
                        'productid',
                        'webaccountid'
                    ))
                    ->join('account', 'account.accountid = ipaddress_pool.accountid', array(
                        'firstname',
                        'lastname'
                    ))
                    ->join('product_hosting', 'product_hosting.hostingid = ipaddress_pool.hostingid', array(
                        'name'
                    ))
                    ->join('webaccount', 'webaccount.webaccountid = ipaddress_pool.webaccountid', array(
                        'domain'
                    ))->where->equalTo('ipaddress_pool.poolid', $poolid);
        });

        return $result->toArray();
    }

}

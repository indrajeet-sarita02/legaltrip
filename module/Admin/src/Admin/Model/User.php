<?php

namespace Admin\Model;

class User {

    public $id;
    public $fname;
    public $lname;
    public $image;
    public $email;
    public $usertype;
    public $dob;
    public $aboutus;
    public $otherinfo;
    public $resume;
    public $address;
    public $address2;
    public $city;
    public $state;
    public $country;
    public $zip;
    public $phone;
    public $mobile;
    public $fax;
    public $email1;
    public $summary;
    public $websiteurl;
    public $galleryimage;
    public $isblock;
    public $status;

    public function setPassword($clear_password) {
        $this->password = md5($clear_password);
    }

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->fname = (isset($data['fname'])) ? $data['fname'] : '';
        $this->lname = (isset($data['lname'])) ? $data['lname'] : '';
        $this->image = (isset($data['image'])) ? $data['image'] : '';
        $this->email = (isset($data['email'])) ? $data['email'] : '';
        if (isset($data['password']) && !empty($data['password'])) {
            $this->password = $data['password'];
        }
        $this->usertype = (isset($data['usertype'])) ? $data['usertype'] : '';
        $this->dob = (isset($data['dob'])) ? $data['dob'] : '';
        $this->aboutus = (isset($data['aboutus'])) ? $data['aboutus'] : '';
        $this->otherinfo = (isset($data['otherinfo'])) ? $data['otherinfo'] : '';
        $this->resume = (isset($data['resume'])) ? $data['resume'] : '';
        $this->address = (isset($data['address'])) ? $data['address'] : '';
        $this->address2 = (isset($data['address2'])) ? $data['address2'] : '';
        $this->city = (isset($data['city'])) ? $data['city'] : '';
        $this->state = (isset($data['state'])) ? $data['state'] : '';
        $this->country = (isset($data['country'])) ? $data['country'] : '';
        $this->zip = (isset($data['zip'])) ? $data['zip'] : '';
        $this->phone = (isset($data['phone'])) ? $data['phone'] : '';
        $this->mobile = (isset($data['mobile'])) ? $data['mobile'] : '';
        $this->fax = (isset($data['fax'])) ? $data['fax'] : '';
        $this->email1 = (isset($data['email1'])) ? $data['email1'] : '';
        $this->summary = (isset($data['summary'])) ? $data['summary'] : '';
        $this->websiteurl = (isset($data['websiteurl'])) ? $data['websiteurl'] : '';
        $this->isblock = (isset($data['isblock'])) ? $data['isblock'] : 0;
        if (isset($data['registerdate']) && !empty($data['registerdate'])) {
            $this->registerdate = (isset($data['registerdate'])) ? $data['registerdate'] : '';
        }
        if (isset($data['galleryimage']) && !empty($data['galleryimage'])) {
            $this->galleryimage = (isset($data['galleryimage'])) ? $data['galleryimage'] : '';
        }
//        $this->lastvisitdate = (isset($data['lastvisitdate'])) ? $data['lastvisitdate'] : '';
        if (isset($data['lastupdated']) && !empty($data['lastupdated'])) {
            $this->lastupdated = (isset($data['lastupdated'])) ? $data['lastupdated'] : '';
        }
//        $this->isfeatured = (isset($data['isfeatured'])) ? $data['isfeatured'] : '';
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
//        $this->membershiptype = (isset($data['membershiptype'])) ? $data['membershiptype'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

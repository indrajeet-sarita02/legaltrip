<?php

namespace Admin\Model;

class Blog {

    public $id;
    public $title;
    public $category_id;
    public $content;
    public $created;
    public $status;
    public $author;

    function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : 0;
        $this->title = (isset($data['title'])) ? $data['title'] : '';
        $this->category_id = (isset($data['category_id'])) ? $data['category_id'] : '';
        $this->content = (isset($data['content'])) ? $data['content'] : '';
        $this->created = (isset($data['created'])) ? $data['created'] : 0;
        $this->status = (isset($data['status'])) ? $data['status'] : 0;
        $this->author = (isset($data['author'])) ? $data['author'] : 0;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

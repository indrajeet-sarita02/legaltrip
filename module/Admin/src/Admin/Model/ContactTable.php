<?php

namespace Admin\Model;

use Zend\Text\Table\Row;
#use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ContactTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($query = null) {
        $adapter = $this->tableGateway->getAdapter();
        // create a new Select object for the table user
        $select = new Select('contact');
        $select->where->in('status', array(0, 1));
        if ($query) {
            $query = trim($query);
            $select->where->equalTo('mobile', $query);
            $select->where->AND->nest()->like('name', '%' . $query . '%')->OR->like('email', '%' . $query . '%')->OR->like('subject', '%' . $query . '%')->OR->like('message', '%' . $query . '%');
        }
        $select->order(array('id DESC'));
        //echo $select->getSqlString();
        // create a new result set based on the Contact entity
        $resultSetPrototype = new ResultSet();
        //$resultSetPrototype->setArrayObjectPrototype(new Contact());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
                // our configured select object
                $select,
                // the adapter to run it against
                $adapter,
                // the result set to hydrate
                $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function saveContact($data) {
        $saveData = array();
        if (isset($data->name) && !empty($data->name)) {
            $saveData['name'] = $data->name;
        }

        if (isset($data->email) && !empty($data->email)) {
            $saveData['email'] = $data->email;
        }

        if (isset($data->mobile) && !empty($data->mobile)) {
            $saveData['mobile'] = $data->mobile;
        }

        if (isset($data->subject) && !empty($data->subject)) {
            $saveData['subject'] = $data->subject;
        }

        if (isset($data->message) && !empty($data->message)) {
            $saveData['message'] = $data->message;
        }

        if (isset($data->type)) {
            $saveData['type'] = $data->type;
        }

        if (isset($data->status)) {
            $saveData['status'] = $data->status;
        }

        $id = (int) $data->id;
        if ($id == 0) {
            $saveData['created'] = time();
            $this->tableGateway->insert($saveData);
        } else {
            if ($this->getContact($id)) {
                $saveData['updated'] = time();
                $this->tableGateway->update($saveData, array('id' => $id));
            } else {
                throw new \Exception('Contact ID does not exist');
            }
        }
    }

    /**
     * Get Contact by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function getContact($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return;
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Get Contact by Id
     * @param string $id
     * @throws \Exception
     * @return Row
     */
    public function userContact($author) {
        $rowset = $this->tableGateway->select(array('author' => $author, 'status' => array(0, 1)));
        if (!$rowset) {
            return;
            throw new \Exception("Could not find row $author");
        }
        return $rowset;
    }

    /**
     * Get mobile account by name
     * @param string $name
     * @throws \Exception
     * @return Row
     */
    public function getContactByName($name) {
        $rowset = $this->tableGateway->select(array('name' => $name));
        $row = $rowset->current();
        if (!$row) {
            return false;
//            throw new \Exception("Could not find row $userEmail");
        }
        return $row;
    }

    /**
     * update Update password
     * @return true/false
     */
    public function updateFields($data, $id) {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (isset($row->id) && !empty($row->id)) {
            $this->tableGateway->update($data, array('id' => $id));
            return $row;
        }
        return false;
    }

    /**
     * Delete Contact by Id
     * @param string $id
     */
    public function deleteContact($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}

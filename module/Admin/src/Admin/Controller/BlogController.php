<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Admin\Form\BlogForm;
use Admin\Form\BlogFilter;

class BlogController extends AbstractActionController {

    public function indexAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $query = $this->params()->fromQuery('q', '');
        $adminSession['query'] = $query;
        $paginator = $this->getServiceLocator()->get('BlogTable')->fetchAll($query);
        // set the current page to what has been passed in query string, or to 1 if none set
        $page = $this->params()->fromQuery('page', 1);
        $paginator->setCurrentPageNumber((int) $page);
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(ITEM_PERPAGE);
        if ($page > 1) {
            $page = ((($page - 1) * ITEM_PERPAGE) + 1);
        }
        return new ViewModel(array('results' => $paginator, 'page' => $page));
    }

    public function editAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $arrCategory = array();
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blogCategory = $blogTable->getCategory();
        foreach ($blogCategory as $val) {
            $arrCategory[$val->id] = $val->title;
        }
        $form = new \Admin\Form\BlogForm($arrCategory);
        if (!empty($this->params()->fromRoute('id'))) {
            $blog = $blogTable->getBlog($this->params()->fromRoute('id'));
            $form->bind($blog);
        }

        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as indicated below.', 'form' => $form,));
            }
            // Save user
            $blog = new \Admin\Model\Blog();
            $blog->exchangeArray($post);
            $blog->author = $adminSession->fname . ' ' . $adminSession->lname;
            $blogTable->saveBlog($blog);
            return $this->redirect()->toRoute('admin/blog');
        }
        return $model;
    }

    public function addAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $arrCategory = array();
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blogCategory = $blogTable->getCategory();
        foreach ($blogCategory as $val) {
            $arrCategory[$val->id] = $val->title;
        }
        $form = new \Admin\Form\BlogForm($arrCategory);
        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as 
                    indicated below.', 'form' => $form,));
            }
            $title = $post['title'];
            $checkData = $blogTable->getBlogByTitle($title);
            if (isset($checkData->title) && !empty($checkData->title)) {
                $model = new ViewModel(array('error' => 'Title already Exists.', 'form' => $form));
                return $model;
            } else {
                // Create user
                $blog = new \Admin\Model\Blog();
                $blog->exchangeArray($post);
                $blog->author = $adminSession->fname . ' ' . $adminSession->lname;
                $blogTable->saveBlog($blog);
                return $this->redirect()->toRoute('admin/blog');
            }
        }
        return $model;
    }

    public function deleteAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $id = $this->params()->fromRoute('id');
        $post['status'] = '-1';
        $userStaus = $blogTable->updateFields($post, $id);
        if ($userStaus) {
            return $this->redirect()->toRoute('admin/blog');
        }
    }

    public function commentsAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $id = $this->params()->fromRoute('id');
        if (empty($id)) {
            return $this->redirect()->toRoute('admin/blog');
        }
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blogComment = $blogTable->getComment($id, 1000, 'admin');
        return new ViewModel(array('blogComment' => $blogComment));
    }

    public function updateCommentStatusAction() {
        if ($this->request->isPost()) {
            $staus = false;
            $msg = '';
            $post = $this->request->getPost();
            // update comment data
            $id = isset($post->id) ? $post->id : '';
            $status = isset($post->status) ? $post->status : '0';
            $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $data['status'] = $status;
            $contactTable = new \Zend\Db\TableGateway\TableGateway('comment', $this->dbAdapter);
            $msg = $contactTable->update($data, array('id' => $id));
            if ($msg) {
                $staus = true;
                $msg = 'Your comment has been successfully updated.';
            }
            echo json_encode(array('status' => $staus, 'msg' => $msg));
            exit;
        }
    }

}

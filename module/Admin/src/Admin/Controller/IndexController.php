<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;
use Zend\Form\Form;

class IndexController extends AbstractActionController {

    public function indexAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $paginator = $this->getServiceLocator()->get('AdminUserTable')->fetchAll();
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(12);
        return new ViewModel(array('users' => $paginator));
    }

}

<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Admin\Form\SignupForm;
use Admin\Form\SignupFilter;
use Admin\Model\User;
use Admin\Model\UserTable;

class UserManagerController extends AbstractActionController {

    public function indexAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $query = $this->params()->fromQuery('q', '');
        $adminSession['query'] = $query;
        $paginator = $this->getServiceLocator()->get('AdminUserTable')->fetchAll($query);
        // set the current page to what has been passed in query string, or to 1 if none set
        $page = $this->params()->fromQuery('page', 1);
        $paginator->setCurrentPageNumber((int) $page);
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(ITEM_PERPAGE);
        if ($page > 1) {
            $page = ((($page - 1) * ITEM_PERPAGE) + 1);
        }
        return new ViewModel(array('users' => $paginator, 'page' => $page));
    }

    public function editAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }

        $userTable = $this->getServiceLocator()->get('AdminUserTable');
        $mstCountryRows = $this->getServiceLocator()->get('UserTable')->getMstCountry();
        $countryData = array();
        foreach ($mstCountryRows as $val) {
            $countryData[$val['name']] = $val['name'];
        }
        $dataParams['countryData'] = $countryData;

        $mstStateRows = $this->getServiceLocator()->get('UserTable')->getMstState();
        $stateData = array();
        foreach ($mstStateRows as $val) {
            $stateData[$val['name']] = $val['name'];
        }
        $dataParams['stateData'] = $stateData;
        $mstCityRows = $this->getServiceLocator()->get('UserTable')->getMstCity();
        $cityData = array();
        foreach ($mstCityRows as $val) {
            $cityData[$val['name']] = $val['name'];
        }
        $dataParams['cityData'] = $cityData;
        $form = new \Admin\Form\UserEditForm($dataParams);
        $uid = $this->params()->fromRoute('id');
        if (!empty($uid)) {
            $user = $userTable->getUser($uid);
            $form->bind($user);
        }

        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $uid = $post['id'];
            $uploadFile = $this->params()->fromFiles('image');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/images";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $user = $userTable->getUser($uid);
                $imageExists = $uploadPath . "/" . $user->image;
                if (!empty($user->image) && file_exists($imageExists)) {
                    @unlink($imageExists);
                }
                $post['image'] = $newName;
            }
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as indicated below.', 'form' => $form,));
            }
            // Save user
            $user = new \Admin\Model\User();
            $user->exchangeArray($post);
            $userTable->saveUser($user);
            return $this->redirect()->toRoute('admin/user-manager', array('action' => 'professionalskill', 'id' => $uid));
        }
        return $model;
    }

    public function addAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $mstCountryRows = $this->getServiceLocator()->get('UserTable')->getMstCountry();
        $countryData = array();
        foreach ($mstCountryRows as $val) {
            $countryData[$val['name']] = $val['name'];
        }
        $dataParams['countryData'] = $countryData;
        $mstStateRows = $this->getServiceLocator()->get('UserTable')->getMstState();
        $stateData = array();
        foreach ($mstStateRows as $val) {
            $stateData[$val['name']] = $val['name'];
        }
        $dataParams['stateData'] = $stateData;
        $mstCityRows = $this->getServiceLocator()->get('UserTable')->getMstCity();
        $cityData = array();
        foreach ($mstCityRows as $val) {
            $cityData[$val['name']] = $val['name'];
        }
        $dataParams['cityData'] = $cityData;
        $form = new \Admin\Form\SignupForm($dataParams);
        $model = new ViewModel(array('form' => $form));
        $userTable = $this->getServiceLocator()->get('AdminUserTable');
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $post['image'] = '';
            $uploadFile = $this->params()->fromFiles('image');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/images";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $post['image'] = $newName;
            }

            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as indicated below.', 'form' => $form,));
            }
            $userEmail = $post['email'];
            $checkData = $userTable->getUserByEmail($userEmail);
            if (isset($checkData->email) && !empty($checkData->email)) {
                $model = new ViewModel(array('error' => 'User already Exists.', 'form' => $form));
                return $model;
            } else {
                // Create user
                $user = new \Admin\Model\User();
                $post['password'] = md5(substr(uniqid(), 0, 6));
                $user->exchangeArray($post);
                //$user->setPassword($post['password']);
                $id = $userTable->saveUser($post);
                $subject = 'Reset password email';
                $email = $post['email'];
                $message = "Hi, <br><br>This is your username:{$email}  and password:{$password} <br>Please login and change your system generated password.<br><br>Thanks,<br><br>Legaltrip Team";
                $this->sendEmail($subject, $message, $email);
                return $this->redirect()->toRoute('admin/user-manager', array('action' => 'professionalskill', 'id' => $id));
            }
        }
        return $model;
    }

    public function professionalskillAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $userTable = $this->getServiceLocator()->get('UserTable');
        $dataParams = array();
        // master case category data
        $mstCaseCatRows = $userTable->getMstCaseCategory();
        $caseCatData = array();
        foreach ($mstCaseCatRows as $val) {
            $caseCatData[$val['id']] = $val['title'];
        }
        $dataParams['mstCaseCat'] = $caseCatData;

        // master case court data
        $mstCaseCourtRows = $userTable->getMstCaseCourt();
        $caseCourtData = array();
        foreach ($mstCaseCourtRows as $val) {
            $caseCourtData[$val['id']] = $val['title'];
        }
        $dataParams['mstCaseCourt'] = $caseCourtData;

        // master case skill data
        $mstSkillRows = $userTable->getMstSkill();
        $skillData = array();
        foreach ($mstSkillRows as $val) {
            $skillData[$val['id']] = $val['title'];
        }
        $dataParams['mstSkill'] = $skillData;

        $form = new \Admin\Form\ProfessionalSkillForm($dataParams);
        $model = new ViewModel(array('form' => $form,));
        $uid = $this->params()->fromRoute('id');
        if (!empty($uid)) {
            $setData = array();
            $selectLawyerCategory = $userTable->selectLawyerCategory($uid);
            $lawyerCatData = array();
            foreach ($selectLawyerCategory as $val) {
                $lawyerCatData[$val['category_id']] = $val['category_id'];
            }
            $setData['category_id'] = $lawyerCatData;
            $selectCaseCourt = $userTable->selectLawyerCourt($uid);
            $lawyerCourtData = array();
            foreach ($selectCaseCourt as $val) {
                $lawyerCourtData[$val['court_id']] = $val['court_id'];
            }
            $setData['court_id'] = $lawyerCourtData;
            $selectSkill = $userTable->selectLawyerSkill($uid);
            $lawyerSkillData = array();
            foreach ($selectSkill as $val) {
                $lawyerSkillData[$val['skill_id']] = $val['skill_id'];
            }
            $setData['skill_id'] = $lawyerSkillData;
            $user = $userTable->getUser($uid);
            /* $user = (array) $user;
              $user = array_merge($user, $setData);
              $user = (object) $user;
              pr($user); */
            if (isset($uid) && !empty(isset($uid))) {
                $form->bind($user);
            }
            $model = new ViewModel(array('form' => $form, 'setData' => $setData));
        }

        if ($this->request->isPost()) {
            $saveData = array();
            $post = $this->request->getPost();
            $uploadFile = $this->params()->fromFiles('resume');
            // Fetch Configuration from Module Config
            $uploadPath = BASE_DIR . "/uploads/resume";
            // Save Uploaded file    	
            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setDestination($uploadPath);
            $newName = time() . "_" . $uploadFile['name'];
            $adapter->addFilter('File\Rename', array(
                'target' => $uploadPath . '/' . $newName,
            ));
            if ($adapter->receive($uploadFile['name'])) {
                $resumeExists = $uploadPath . "/" . $user->resume;
                if (!empty($user->resume) && file_exists($resumeExists)) {
                    @unlink($resumeExists);
                }
                $saveData['resume'] = $newName;
            }
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form,));
            }
            $uid = $saveData['id'] = $post['id'];
            if (!empty($post['websiteurl'])) {
                $saveData['websiteurl'] = $post['websiteurl'];
            }
            if (!empty($post['summary'])) {
                $saveData['summary'] = $post['summary'];
            }
            // Save Lawyer Category
            $userTable->deleteCaseCategory($uid);
            $userTable->saveCaseCategory($post['category_id'], $uid);
            // Save Lawyer Court
            $userTable->deleteCaseCourt($uid);
            $userTable->saveCaseCourt($post['court_id'], $uid);
            // Save Lawyer Skill
            $userTable->deleteSkill($uid);
            $userTable->saveSkill($post['skill_id'], $uid);
            // update user
            if (count($saveData) > 1) {
                $user = new \Admin\Model\User();
                $user->exchangeArray($saveData);
                $this->getServiceLocator()->get('AdminUserTable')->saveUser($user);
            }
            return $this->redirect()->toRoute('admin/user-manager', array('action' => 'index', 'msg' => 1));
        }
        return $model;
    }

    public function deleteAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $userTable = $this->getServiceLocator()->get('AdminUserTable');
        $id = $this->params()->fromRoute('id');
        $post['status'] = '-1';
        $userStaus = $userTable->updateFields($post, $id);
        if ($userStaus) {
            return $this->redirect()->toRoute('admin/user-manager');
        }
    }

    public function resetPasswordAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $userTable = $this->getServiceLocator()->get('AdminUserTable');
        $msg = '';
        $staus = false;
        $id = $this->params()->fromQuery('id');
        if ($id) {
            $post = array();
            $password = substr(uniqid(), 0, 6);
            $post['password'] = md5($password);
            $userStaus = $userTable->updateFields($post, $id);
            if ($userStaus) {
                $subject = 'Reset password email';
                $email = $userStaus->email;
                $message = "Hi, <br><br>This is your username:{$email}  and password:{$password} <br>Please login and change your system generated password.<br><br>Thanks,<br><br>Legaltrip Team";
                $this->sendEmail($subject, $message, $email);
                $staus = true;
            }
            $msg = 'Password has been send on your register email.';
        }
        echo json_encode(array('status' => $staus, 'msg' => $msg));
        exit();
    }

    public function sendEmail($subject, $message, $email) {
        $objEmail = new \Zend\Mail\Message();
        $objEmail->setBody($message);
        $objEmail->setFrom('indrajeet.sarita02@gmail.com', 'From');
        $objEmail->addTo($email, 'To');
        $objEmail->setSubject($subject);
//                $transport = new \Zend\Mail\Transport\Sendmail(/* '-freturn_to_me@example.com' */);
//                $transport->send($mail);
        // Setup SMTP transport using PLAIN authentication over TLS
        $transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions(array(
            'name' => 'smtp.gmail.com',
            'host' => 'smtp.gmail.com',
            'port' => 587, // Notice port change for TLS is 587
            'connection_class' => 'plain',
            'connection_config' => array(
                'username' => '',
                'password' => '',
                'ssl' => 'tls',
            ),
        ));
        $transport->setOptions($options);
        //$transport->send($objEmail);
    }

    public function getCityByStateAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $msg = '';
        $staus = false;
        $name = $this->params()->fromQuery('name');
        if ($name) {
            $mstCityRows = $this->getServiceLocator()->get('UserTable')->getCityByState($name);
            $cityData = '';
            foreach ($mstCityRows as $val) {
                $cityData .= '<option value="' . $val['name'] . '">' . $val['name'] . '</option>';
            }
            $msg = $cityData;
            $staus = true;
        }
        echo json_encode(array('status' => $staus, 'msg' => $msg));
        exit();
    }

}

<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Admin\Form\LoginForm;
use Admin\Form\LoginFilter;
use Admin\Model\User;
use Admin\Model\UserTable;

class LoginController extends AbstractActionController {

    protected $storage;
    protected $authservice;

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function logoutAction() {
        $this->getAuthService()->clearIdentity();
        $adminSession = new \Zend\Session\Container('adminUser');
        $adminSession->getManager()->getStorage()->clear('adminUser');
        return $this->redirect()->toRoute('admin/login');
    }

    public function indexAction() {
        $form = $this->getServiceLocator()->get('AdminLoginForm');
        $viewModel = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $viewModel = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form));
            } else {
                //check authentication...
                $this->getAuthService()->getAdapter()
                        ->setIdentity($post['email'])
                        ->setCredential($post['password']);
                $result = $this->getAuthService()->authenticate();
                if ($result->isValid()) {
                    $this->getAuthService()->getStorage()->write($post['email']);
                    $userTable = $this->getServiceLocator()->get('UserTable');
                    $userData = $userTable->getUserByEmail($post['email']);
                    $adminSession = new \Zend\Session\Container('adminUser');
                    $adminSession->id = $userData->id;
                    $adminSession->fname = $userData->fname;
                    $adminSession->lname = $userData->lname;
                    $adminSession->email = $userData->email;
                    $adminSession->mobile = $userData->mobile;
                    $adminSession->usertype = $userData->usertype;
                    $adminSession->status = $userData->status;
                    if ($userData->usertype == 'admin') {
                        return $this->redirect()->toRoute('admin/index', array('action' => 'index'));
                    } else {
                        $viewModel = new ViewModel(array('error' => 'You are not admin. Please try again!', 'form' => $form,));
                    }
                } else {
                    $viewModel = new ViewModel(array('error' => 'Something going wrong. Please try again!', 'form' => $form));
                }
            }
        }
        return $viewModel;
    }

}

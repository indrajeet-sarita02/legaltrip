<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use Admin\Form\EmailForm;
use Admin\Form\EmailFilter;
#use Admin\Model\User;
#use Admin\Model\UserTable;

class EmailTemplateController extends AbstractActionController {

    public function indexAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $query = $this->params()->fromQuery('q', '');
        $adminSession['query'] = $query;
        $paginator = $this->getServiceLocator()->get('EmailTemplateTable')->fetchAll($query);
        // set the current page to what has been passed in query string, or to 1 if none set
        $page = $this->params()->fromQuery('page', 1);
        $paginator->setCurrentPageNumber((int) $page);
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(ITEM_PERPAGE);
        if ($page > 1) {
            $page = ((($page - 1) * ITEM_PERPAGE) + 1);
        }
        return new ViewModel(array('results' => $paginator, 'page' => $page));
    }

    public function editAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $form = new \Admin\Form\EmailTemplateForm();
        $contentTable = $this->getServiceLocator()->get('EmailTemplateTable');
        if (!empty($this->params()->fromRoute('id'))) {
            $content = $contentTable->getEmailTemplate($this->params()->fromRoute('id'));
            $form->bind($content);
        }

        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as indicated below.', 'form' => $form,));
            }
            // Save user
            $content = new \Admin\Model\EmailTemplate();
            $content->exchangeArray($post);
            $contentTable->saveEmailTemplate($content);
            return $this->redirect()->toRoute('admin/email-template');
        }
        return $model;
    }

    public function addAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $form = new \Admin\Form\EmailTemplateForm();
        $model = new ViewModel(array('form' => $form));
        $contentTable = $this->getServiceLocator()->get('EmailTemplateTable');
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as 
                    indicated below.', 'form' => $form,));
            }
            $title = $post['title'];
            $checkData = $contentTable->getEmailTemplateByTitle($title);
            if (isset($checkData->title) && !empty($checkData->title)) {
                $model = new ViewModel(array('error' => 'Title already Exists.', 'form' => $form));
                return $model;
            } else {
                // Create user
                $content = new \Admin\Model\EmailTemplate();
                $content->exchangeArray($post);
                $contentTable->saveEmailTemplate($post);
                return $this->redirect()->toRoute('admin/email-template');
            }
        }
        return $model;
    }

    public function deleteAction() {
        $adminSession = new \Zend\Session\Container('adminUser');
        if ($adminSession->usertype != 'admin') {
            return $this->redirect()->toRoute('admin/login');
        }
        $contentTable = $this->getServiceLocator()->get('EmailTemplateTable');
        $id = $this->params()->fromRoute('id');
        $post['status'] = '-1';
        $userStaus = $contentTable->updateFields($post, $id);
        if ($userStaus) {
            return $this->redirect()->toRoute('admin/email-template');
        }
    }

}

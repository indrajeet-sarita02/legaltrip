<?php

namespace Admin\Form;

use Zend\Form\Form;

class ContactForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Contact');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'content',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'content',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        
        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'status',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Status',
                'empty_option' => 'Select',
                'value_options' => array('1' => 'Active', '0' => 'InActive'),
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Submit'
            ),
        ));
    }

}

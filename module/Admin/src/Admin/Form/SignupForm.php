<?php

namespace Admin\Form;

use Zend\Form\Form;

class SignupForm extends Form {

    public function __construct($params = null) {
        parent::__construct('Signup');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'fname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fname',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'First Name',
            ),
        ));

        $this->add(array(
            'name' => 'lname',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lname',
                'class' => 'form-control1',
               // 'required' => 'required'
            ),
            'options' => array(
                'label' => 'Last Name',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'id' => 'email',
                'class' => 'form-control1',
                'required' => 'required',
            //'readonly' => 'readonly'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        /* $this->add(array(
          'name' => 'password',
          'attributes' => array(
          'type' => 'password',
          'id' => 'password',
          'class' => 'form-control1',
          'required' => 'required'
          ),
          'options' => array(
          'label' => 'Password',
          ),
          )); */

        $this->add(array(
            'name' => 'dob',
            'attributes' => array(
                'type' => 'text',
                'id' => 'dob',
                'class' => 'form-control1',
                'readonly' => 'readonly',
            ),
            'options' => array(
                'label' => 'Date of birth',
            ),
        ));

        $this->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
                'id' => 'mobile',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Mobile',
            ),
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Phone No.',
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));
        $this->add(array(
            'name' => 'address2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address2',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Other Address',
            ),
        ));

        $this->add(array(
            'name' => 'aboutus',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'aboutus',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'About Us',
            ),
        ));

        $this->add(array(
            'name' => 'otherinfo',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'otherinfo',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Other Info',
            ),
        ));

        $this->add(array(
            'name' => 'fax',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fax',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Fax No.',
            ),
        ));

        $this->add(array(
            'name' => 'email1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email1',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Other Email',
            ),
        ));

        $this->add(array(
            'name' => 'image',
            'attributes' => array(
                'type' => 'file',
                'id' => 'image',
                'class' => 'form-control-file',
            ),
            'options' => array(
                'label' => 'Image',
            ),
        ));

        $this->add(array(
            'name' => 'country',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'country',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Country',
                'empty_option' => 'Select',
                'value_options' => $params['countryData'],
            ),
        ));

        $this->add(array(
            'name' => 'state',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'state',
                'class' => 'form-control1',
                'onchange' => 'javascript: getCityByState(this);',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'State',
                'empty_option' => 'Select',
                'value_options' => $params['stateData'],
            ),
        ));

        $this->add(array(
            'name' => 'city',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'city',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'City',
                'empty_option' => 'Select',
                'value_options' => $params['cityData'],
            ),
        ));

        $this->add(array(
            'name' => 'usertype',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'usertype',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'User Type',
                'empty_option' => 'Select',
                'value_options' => (array) json_decode(USER_TYPE),
            ),
        ));
        
        $this->add(array(
            'name' => 'zip',
            'attributes' => array(
                'type' => 'text',
                'id' => 'zip',
                'class' => 'form-control1',
            ),
            'options' => array(
                'label' => 'Zipcode',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Submit'
            ),
        ));
    }

}

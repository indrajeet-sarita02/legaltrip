<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;

class EmailTemplateFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'emailto',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'emailfrom',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'title',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'subject',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'identifire',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'content',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'keyword',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'status',
            'required' => true,
        ));
    }

}

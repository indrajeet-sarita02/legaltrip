<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;

class ContentFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'title',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'content',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'meta_title',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'meta_keyword',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'meta_description',
            'required' => true,
        ));
        $this->add(array(
            'name' => 'status',
            'required' => true,
        ));
    }

}

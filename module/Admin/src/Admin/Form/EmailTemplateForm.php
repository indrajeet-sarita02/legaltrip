<?php

namespace Admin\Form;

use Zend\Form\Form;

class EmailTemplateForm extends Form {

    public function __construct($name = null) {
        parent::__construct('EmailTemplate');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'emailto',
            'attributes' => array(
                'type' => 'text',
                'id' => 'emailto',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Email To',
            ),
        ));
        $this->add(array(
            'name' => 'emailfrom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'emailfrom',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Email From',
            ),
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'subject',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Subject',
            ),
        ));
        $this->add(array(
            'name' => 'identifire',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'identifire',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Identifire',
            ),
        ));
        $this->add(array(
            'name' => 'content',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'content',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        $this->add(array(
            'name' => 'keyword',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'keyword',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Keyword',
            ),
        ));
        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'status',
                'class' => 'form-control1',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Status',
                'empty_option' => 'Select',
                'value_options' => array('1' => 'Active', '0' => 'InActive'),
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-success',
                'value' => 'Submit'
            ),
        ));
    }

}

<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;

class BlogFilter extends InputFilter {

    public function __construct() {
        $this->add(array(
            'name' => 'title',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'content',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'status',
            'required' => true,
        ));
    }

}

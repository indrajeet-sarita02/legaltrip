<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;

class SignupFilter extends InputFilter
{

    public function __construct()
    {
        $this->add(array(
            'name' => 'fname',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'lname',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags',
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 140,
                    ),
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                        'domain' => true,
                    ),
                ),
            ),
        ));
        
        /*$this->add(array(
            'name' => 'password',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'confirm_password',
            'required' => true,
        ));*/

        $this->add(array(
            'name' => 'mobile',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'address',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'aboutus',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'otherinfo',
            'required' => true,
        ));

        $this->add(array(
            'name' => 'image',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'usertype',
            'required' => true,
        ));
    }

}

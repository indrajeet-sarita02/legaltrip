<?php

// filename : module/Admin/src/Admin/Form/RegisterForm.php

namespace Admin\Form;

use Zend\Form\Form;

class LoginForm extends Form {

    public function __construct($name = null) {
        parent::__construct('Login');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email',
                'placeholder' => 'E-mail',
                'class' => 'input-control text',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password',
                'placeholder' => 'Password',
                'class' => 'input-control',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));

        $this->add(array(
            'name' => 'remember',
            'attributes' => array(
                'type' => 'hidden',
                'value' => '1'
            ),
            'options' => array(
                'label' => 'Remember Me',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-success btn-block',
                'value' => 'Login'
            ),
        ));
    }

}

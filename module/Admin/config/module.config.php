<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Login' => 'Admin\Controller\LoginController',
            'Admin\Controller\UserManager' => 'Admin\Controller\UserManagerController',
            'Admin\Controller\Content' => 'Admin\Controller\ContentController',
            'Admin\Controller\EmailTemplate' => 'Admin\Controller\EmailTemplateController',
            'Admin\Controller\Contact' => 'Admin\Controller\ContactController',
            'Admin\Controller\Blog' => 'Admin\Controller\BlogController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/admin',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Login',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.           
                    'login' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/login[/:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Login',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'index' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/index[/:action]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Index',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'user-manager' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/user-manager[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\UserManager',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'content' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/content[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Content',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'email-template' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/email-template[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\EmailTemplate',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'contact' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/contact[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Contact',
                                'action' => 'index',
                            ),
                        ),
                    ),
                    'blog' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/blog[/:action[/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Admin\Controller\Blog',
                                'action' => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'layout/login' => BASE_DIR . '/view/layout/admin-login-layout.phtml',
            'layout/admin' => BASE_DIR . '/view/layout/admin-layout.phtml',
            'error/404' => BASE_DIR . '/view/error/404.phtml',
            'error/index' => BASE_DIR . '/view/error/index.phtml',
        ),
    ),
    // MODULE CONFIGURATIONS
    'module_config' => array(
        'upload_location' => BASE_DIR . '/uploads/documents',
        'image_upload_location' => BASE_DIR . '/uploads/images',
        'search_index' => BASE_DIR . '/uploads/search_index'
    ),
    'speck-paypal-api' => array(
        'username' => '<USERNAME>',
        'password' => '<PASSWORD>',
        'signature' => '<SIGNATURE>',
        'endpoint' => 'https://api-3t.sandbox.paypal.com/nvp'
    )
);

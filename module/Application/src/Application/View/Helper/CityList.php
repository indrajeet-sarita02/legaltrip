<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Facebook\Facebook;

class CityList extends AbstractHelper {

    public function __construct($adapter, $cache) {
        $this->adapter = $adapter;
        $this->cache = $cache;
    }

    public function getAllCity() {
        // set unique Cache key
        $cityKey = md5('getAllCity');
        // get the Cache data
        //$this->cache->clean();
        $cityResult = $this->cache->getItem($cityKey, $citySuccess);
        if (!$citySuccess) {
            $sql = new Sql($this->adapter);
            $select = $sql->select()->from(array('mc' => 'mst_city'));
            $select->order(array('name ASC'));
            $select->limit(1000000000);
            $statement = $sql->prepareStatementForSqlObject($select);
            $rec = $statement->execute();
            $arrCity = array();
            foreach ($rec as $val) {
                $arrCity[$val['id']] = $val['name'];
            }
            $this->cache->setItem($cityKey, $arrCity);
        } else {
            $arrCity = $cityResult;
        }
        return $arrCity;
    }

    public function getPopularLawyer() {
        // set unique Cache key
        $popularKey = md5('getPopularLawyer');
        // get the Cache data
        //$this->cache->clean();
        $popularResult = $this->cache->getItem($popularKey, $popularSuccess);
        if ($popularSuccess) {
            $sql = new Sql($this->adapter);
            $select = $sql->select()->from(array('sd' => 'search_data'));
            $select->where(array('isfeatured' => 1));
            $rand = new \Zend\Db\Sql\Expression('RAND()');
            $select->order($rand);
            $select->limit(10);
            $query = $sql->prepareStatementForSqlObject($select);
            //print($query->getSql());
            $resutl = $query->execute();
//            $resultSet = new ResultSet();
//            $resultSet->initialize($resutl);
            $arrPopular = array();
            foreach ($resutl as $key => $val) {
                $arrPopular[$key]['id'] = $val['id'];
                $arrPopular[$key]['name'] = $val['name'];
                $arrPopular[$key]['image'] = $val['image'];
                $zip = !empty($val['zip']) ? $val['zip'] : "";
                $arrPopular[$key]['address'] = ucwords($val['address'] . " " . $val['city'] . " " . $val['state'] . " " . $zip);
//                $arrPopular[$key]['id'] = $val['name'];
            }
            $this->cache->setItem($popularKey, $arrPopular);
            $resultSet = $arrPopular;
        } else {
            $resultSet = $popularResult;
        }
        return $resultSet;
    }

    public function facebookLogin($login = false) {
        ######### Facebook API Configuration ##########
        if ($login == true) {
            $loginTxt = 'Login with Facebook';
        } else {
            $loginTxt = 'Connect with Facebook';
        }
//        $appId = '815960145183239'; //Facebook App ID
//        $appSecret = '6d973751b6d977a6a6f3b17561f13199'; // Facebook App Secret
        $homeurl = BASE_URL;  //return to home
        $fbPermissions = 'email';  //Required facebook permissions
        //Call Facebook API
        $facebook = new \Zend\Facebook\Facebook(array(
            'appId' => FB_APPID,
            'secret' => FB_APPSECRET
        ));
        $fbuser = $facebook->getUser();
        //destroy facebook session if user clicks reset
        if (!$fbuser) {
            $fbuser = null;
            $loginUrl = $facebook->getLoginUrl(array('redirect_uri' => $homeurl, 'scope' => $fbPermissions));
            $output = '<a href="' . $loginUrl . '">' . $loginTxt . '</a>';
        } else {
            $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
            $userData = $this->checkSocialUser('facebook', $userProfile['id'], $userProfile['first_name'], $userProfile['last_name'], $userProfile['email'], $userProfile['gender'], $userProfile['locale'], $userProfile['picture']['data']['url']);
            if (!empty($userData)) {
                $userSession = new \Zend\Session\Container('siteUser');
                $userSession->id = $userData['id'];
                $userSession->fname = $userData['fname'];
                $userSession->lname = $userData['lname'];
                $userSession->email = $userData['email'];
                $userSession->mobile = $userData['mobile'];
                $userSession->usertype = $userData['usertype'];
                $userSession->oauth_provider = $userData['oauth_provider'];
                $userSession->status = $userData['status'];
                $output = '';
            } else {
                $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
            }
        }
        return array($fbuser, $output);
    }

    public function googleLogin($login = false) {
        require_once (BASE_DIR . "/../vendor/Google/autoload.php");
        $userSession = new \Zend\Session\Container('siteUser');
        ######### Google API Configuration ##########
        if ($login == true) {
            $loginTxt = 'Login with Google';
        } else {
            $loginTxt = 'Connect with Google';
        }
//        $clientId = '366631035754-tbrsvfm4pmnilvvrsgd7l33kvj79t5c2.apps.googleusercontent.com';
//        $clientSecret = 'fYgHW9IVtvN0EcaqzbgI_rMn';
        $homeurl = BASE_URL;  //return to home

        /*         * **********************************************
          Make an API request on behalf of a user. In
          this case we need to have a valid OAuth 2.0
          token for the user, so we need to send them
          through a login flow. To do this we need some
          information from our API console project.
         * ********************************************** */
        $client = new \Google_Client();
        $client->setClientId(GOOGLE_APPID);
        $client->setClientSecret(GOOGLE_APPSECRET);
        $client->setRedirectUri($homeurl);
        $client->addScope("email");
        $client->addScope("profile");

        /**         * *********************************************
          When we create the service here, we pass the
          client to it. The client then queries the service
          for the required scopes, and uses that when
          generating the authentication URL later.
         * ********************************************** */
        $service = new \Google_Service_Oauth2($client);
        if (isset($_GET['code']) && empty($_GET['state'])) {
            $client->authenticate($_GET['code']);
            $userSession->accessToken = $client->getAccessToken();
        }
        /*         * **********************************************
          If we have an access token, we can make
          requests, else we generate an authentication URL.
         * ********************************************** */
        $authUrl = '';
        if (isset($userSession->accessToken) && !empty($userSession->accessToken)) {
            $client->setAccessToken($userSession->accessToken);
        } else {
            $authUrl = $client->createAuthUrl();
        }

        //destroy facebook session if user clicks reset
        if (isset($authUrl) && !empty($authUrl)) {
            $guser = null;
            $output = '<a href="' . $authUrl . '">' . $loginTxt . '</a>';
        } else {
            $guser = 1;
            $user = $service->userinfo->get(); //get user info 
            $userProfile = (array) $user;
            $userData = $this->checkSocialUser('google', $userProfile['id'], $userProfile['givenName'], $userProfile['familyName'], $userProfile['email'], $userProfile['gender'], $userProfile['locale'], $userProfile['picture']);
            if (!empty($userData)) {
                $userSession->id = $userData['id'];
                $userSession->fname = $userData['fname'];
                $userSession->lname = $userData['lname'];
                $userSession->email = $userData['email'];
                $userSession->mobile = $userData['mobile'];
                $userSession->usertype = $userData['usertype'];
                $userSession->oauth_provider = $userData['oauth_provider'];
                $userSession->status = $userData['status'];
                $output = '';
            } else {
                $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
            }
        }
        return array($guser, $output);
    }

    public function microsoftLogin($login = false) {
        ######### Microsoft API Configuration ##########
        if ($login == true) {
            $loginTxt = 'Login with Microsoft';
        } else {
            $loginTxt = 'Connect with Microsoft';
        }

        $homeurl = BASE_URL;  //return to home
        $client = new oauth_client_class;
        $client->server = 'Microsoft';
        $client->redirect_uri = $homeurl;

        $client->client_id = MICROSOFT_APPID;
        $application_line = __LINE__;
        $client->client_secret = MICROSOFT_APPSECRET;
        if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0) {
            die('Please go to Microsoft Live Connect Developer Center page ' .
                    'https://manage.dev.live.com/AddApplication.aspx and create a new' .
                    'application, and in the line ' . $application_line .
                    ' set the client_id to Client ID and client_secret with Client secret. ' .
                    'The callback URL must be ' . $client->redirect_uri . ' but make sure ' .
                    'the domain is valid and can be resolved by a public DNS.');
        }
        /* API permissions
         */
        $client->scope = 'wl.basic wl.emails wl.birthday';
        if (($success = $client->Initialize())) {
            if (($success = $client->Process())) {
                if (strlen($client->authorization_error)) {
                    $client->error = $client->authorization_error;
                    $success = false;
                } elseif (strlen($client->access_token)) {
                    $success = $client->CallAPI('https://apis.live.net/v5.0/me', 'GET', array(), array('FailOnAccessError' => true), $user);
                }
            }
            $success = $client->Finalize($success);
        }
        if ($client->exit)
            exit;
        if ($success) {
            session_start();
            $_SESSION['userdata'] = $user;
            header("location: home.php");
        } else {
            echo 'Error:' . HtmlSpecialChars($client->error);
        }
        //destroy facebook session if user clicks reset
        if (!$success) {
            $fbuser = null;
            $loginUrl = $facebook->getLoginUrl(array('redirect_uri' => $homeurl, 'scope' => $fbPermissions));
            $output = '<a href="' . $loginUrl . '">' . $loginTxt . '</a>';
        } else {
            $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
            $userData = $this->checkSocialUser('facebook', $userProfile['id'], $userProfile['first_name'], $userProfile['last_name'], $userProfile['email'], $userProfile['gender'], $userProfile['locale'], $userProfile['picture']['data']['url']);
            if (!empty($userData)) {
                $userSession = new \Zend\Session\Container('siteUser');
                $userSession->id = $userData['id'];
                $userSession->fname = $userData['fname'];
                $userSession->lname = $userData['lname'];
                $userSession->email = $userData['email'];
                $userSession->mobile = $userData['mobile'];
                $userSession->usertype = $userData['usertype'];
                $userSession->oauth_provider = $userData['oauth_provider'];
                $userSession->status = $userData['status'];
                $output = '';
            } else {
                $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
            }
        }
        return array($fbuser, $output);
    }

    public function linkedinLogin($login = false) {
        ######### LinkedIn API Configuration ##########
        if ($login == true) {
            $loginTxt = 'Login with LinkedIn';
        } else {
            $loginTxt = 'Connect with LinkedIn';
        }

        $homeurl = BASE_URL;  //return to home
        $client = new oauth_client_class;
        $client->debug = false;
        $client->debug_http = true;
        $client->server = 'LinkedIn';
        $client->redirect_uri = $homeurl;

        $client->client_id = LINKEDIN_APPID;
        $application_line = __LINE__;
        $client->client_secret = LINKEDIN_APPSECRET;
        if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0) {
            die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , ' .
                            'create an application, and in the line ' . $application_line .
                            ' set the client_id to Consumer key and client_secret with Consumer secret. ' .
                            'The Callback URL must be ' . $client->redirect_uri) . ' Make sure you enable the ' .
                    'necessary permissions to execute the API calls your application needs.';
        }
        /* API permissions
         */
        $client->scope = 'r_basicprofile r_emailaddress';
        if (($success = $client->Initialize())) {
            if (($success = $client->Process())) {
                if (strlen($client->authorization_error)) {
                    $client->error = $client->authorization_error;
                    $success = false;
                } elseif (strlen($client->access_token)) {
                    $success = $client->CallAPI('http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 'GET', array('format' => 'json'), array('FailOnAccessError' => true), $user);
                }
            }
            $success = $client->Finalize($success);
        }
        if ($client->exit)
            exit;
        if ($success) {
            $user_id = $db->checkUser($user);
            $_SESSION['loggedin_user_id'] = $user_id;
            $_SESSION['user'] = $user;
        } else {
            $_SESSION["err_msg"] = $client->error;
        }
        //destroy facebook session if user clicks reset
        if (!$success) {
            $fbuser = null;
            $loginUrl = $facebook->getLoginUrl(array('redirect_uri' => $homeurl, 'scope' => $fbPermissions));
            $output = '<a href="' . $loginUrl . '">' . $loginTxt . '</a>';
        } else {
            $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
            $userData = $this->checkSocialUser('facebook', $userProfile['id'], $userProfile['first_name'], $userProfile['last_name'], $userProfile['email'], $userProfile['gender'], $userProfile['locale'], $userProfile['picture']['data']['url']);
            if (!empty($userData)) {
                $userSession = new \Zend\Session\Container('siteUser');
                $userSession->id = $userData['id'];
                $userSession->fname = $userData['fname'];
                $userSession->lname = $userData['lname'];
                $userSession->email = $userData['email'];
                $userSession->mobile = $userData['mobile'];
                $userSession->usertype = $userData['usertype'];
                $userSession->oauth_provider = $userData['oauth_provider'];
                $userSession->status = $userData['status'];
                $output = '';
            } else {
                $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
            }
        }
        return array($fbuser, $output);
    }

    public function checkSocialUser($oauth_provider, $oauth_uid, $fname, $lname, $email, $gender, $locale, $picture) {
        $userTable = new \Zend\Db\TableGateway\TableGateway('user', $this->adapter);
        //$userRows = $userTable->select(array('oauth_provider' => "$oauth_provider", 'oauth_uid' => "$oauth_uid"));
        $userRows = $userTable->select(array('email' => $email));
        $row = $userRows->current();
        if (!empty($row->email)) {
            $data = array(
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'gender' => $gender,
                'image' => $picture,
                'usertype' => 'lawyer',
                'lastvisitdate' => time()
            );
            $userTable->update($data, array('oauth_provider' => "$oauth_provider", 'oauth_uid' => "$oauth_uid"));
        } else {
            $data = array(
                'fname' => $fname,
                'lname' => $lname,
                'email' => $email,
                'gender' => $gender,
                'image' => $picture,
                'usertype' => 'lawyer',
                'oauth_provider' => "$oauth_provider",
                'oauth_uid' => "$oauth_uid",
                'registerdate' => time()
            );
            $userTable->insert($data);
        }
        //$userFRows = $userTable->select(array('oauth_provider' => "$oauth_provider", 'oauth_uid' => "$oauth_uid"));
        $userFRows = $userTable->select(array('email' => $email));
        $fRow = $userFRows->current();
        return $fRow;
    }

    public function truncateString($string, $length = 50) {
        $string = stripcslashes($string);
        if (strlen($string) > $length) {
            return $string = ucfirst(trim(substr($string, 0, $length), ",")) . "...";
        } else {
            return ucfirst($string);
        }
    }

    public function cleanUrl($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return trim(strtolower(preg_replace('/-+/', '-', $string)), '-') . ".html"; // Replaces multiple hyphens with single one.
    }

    public function cleanContact($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^0-9]/', '', $string); // Removes special chars.
        return trim(preg_replace('/-+/', '', $string)); // Replaces multiple hyphens with single one.
    }

    public function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

}

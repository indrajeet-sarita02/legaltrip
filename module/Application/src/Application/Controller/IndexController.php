<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    public function indexAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(9);
        return new ViewModel(array('content' => $content));
    }

    public function contactusAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(10);
        // prepare search form
        $form = new \Zend\Form\Form();
        $form->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($name)) ? $name : '',
                'id' => 'name',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $form->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'value' => (isset($email)) ? $email : '',
                'id' => 'email',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $form->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($mobile)) ? $mobile : '',
                'id' => 'mobile',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Mobile',
            ),
        ));
        $form->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($subject)) ? $subject : '',
                'id' => 'subject',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Subject',
            ),
        ));
        $form->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'value' => (isset($message)) ? $message : '',
                'id' => 'message',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Message',
            ),
        ));
        $form->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'submit',
                'class' => 'submit',
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $viewModel = new ViewModel(array('error' => true, 'form' => $form, 'content' => $content));
            }
            // save contact data
            $post->type = 'contactus';
            $this->saveContact($post);
            return $this->redirect()->toUrl(BASE_URL . '/page/contactus?msg=1'); //->toRoute('support/page', array('action' => 'contactus'));
        }

        $viewModel = new ViewModel(array('form' => $form, 'content' => $content));
        return $viewModel;
    }

    public function feedbackAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(11);
        // prepare search form
        $form = new \Zend\Form\Form();
        $form->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($name)) ? $name : '',
                'id' => 'name',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        $form->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'value' => (isset($email)) ? $email : '',
                'id' => 'email',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $form->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($mobile)) ? $mobile : '',
                'id' => 'mobile',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Mobile',
            ),
        ));
        $form->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'value' => (isset($subject)) ? $subject : '',
                'id' => 'subject',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Subject',
            ),
        ));
        $form->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'value' => (isset($message)) ? $message : '',
                'id' => 'message',
                'class' => 'form-control',
                'required' => 'required',
                'placeholder' => ''
            ),
            'options' => array(
                'label' => 'Message',
            ),
        ));
        $form->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'submit',
                'class' => 'submit',
            ),
            'options' => array(
                'label' => ' ',
            ),
        ));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $viewModel = new ViewModel(array('error' => true, 'form' => $form, 'content' => $content));
            }
            // save contact data
            $post->type = 'feedback';
            $this->saveContact($post);
            return $this->redirect()->toUrl(BASE_URL . '/page/feedback?msg=1'); //->toRoute('support/page', array('action' => 'contactus'));
        }

        $viewModel = new ViewModel(array('form' => $form, 'content' => $content));
        return $viewModel;
    }

    public function contactdetailAction() {
        if ($this->request->isPost()) {
            $staus = false;
            $msg = '';
            $post = $this->request->getPost();
            // save contact data
            $chk = 0;
            $chk = $this->saveContact($post);
            if ($chk == true) {
                $staus = true;
                $msg = 'Your message has been sent successfully.';
            }
            echo json_encode(array('status' => $staus, 'msg' => $msg));
            exit;
        }
    }

    public function saveContact($post) {
        // Save education
        $name = isset($post->name) ? $post->name : '';
        $email = isset($post->email) ? $post->email : '';
        $mobile = isset($post->mobile) ? $post->mobile : '';
        $subject = isset($post->subject) ? $post->subject : '';
        $message = isset($post->message) ? $post->message : '';
        $type = isset($post->type) ? $post->type : '';
        $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $data = array(
            'name' => $name,
            'email' => $email,
            'mobile' => $mobile,
            'subject' => $subject,
            'message' => $message,
            'type' => $type,
            'created' => time()
        );
        $contactTable = new \Zend\Db\TableGateway\TableGateway('contact', $this->dbAdapter);
        $msg = $contactTable->insert($data);
        if ($msg == 1) {
            $this->sendEmail($subject, $message);
            /* $objEmail = new \Zend\Mail\Message();
              $objEmail->setBody($message);
              $objEmail->setFrom('indrajeet.sarita02@gmail.com', 'From');
              $objEmail->addTo('indrajeet.sarita02@gmail.com', 'To');
              $objEmail->setSubject($subject);
              //                $transport = new \Zend\Mail\Transport\Sendmail();
              //                $transport->send($mail);
              // Setup SMTP transport using PLAIN authentication over TLS
              $transport = new \Zend\Mail\Transport\Smtp();
              $options = new \Zend\Mail\Transport\SmtpOptions(array(
              'name' => 'smtp.gmail.com',
              'host' => 'smtp.gmail.com',
              'port' => 587, // Notice port change for TLS is 587
              'connection_class' => 'plain',
              'connection_config' => array(
              'username' => '',
              'password' => '',
              'ssl' => 'tls',
              ),
              ));
              $transport->setOptions($options);
              //$transport->send($objEmail); */
        }
        return true;
    }

    public function subscribeEmailAction() {
        if ($this->request->isPost()) {
            $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $staus = false;
            $msg = '';
            $chk = 0;
            $post = $this->request->getPost();
            // subscribe email save
            $email = isset($post->email) ? $post->email : '';
            $data = array(
                'email' => $email,
                'created' => time()
            );
            $subscribeTable = new \Zend\Db\TableGateway\TableGateway('subscribe', $this->dbAdapter);
            $chk = $subscribeTable->insert($data);
            if ($chk == true) {
                $staus = true;
                $msg = 'Your message has been sent successfully.';
                $subject = 'Subscribe email';
                $message = 'Hi, <br>This is subscribe email.<br><br>Thanks,<br><br>Legaltrip Team';
                $this->sendEmail($subject, $message);
            }
            echo json_encode(array('status' => $staus, 'msg' => $msg));
            exit;
        }
    }

    public function sendEmail($subject, $message) {
        $objEmail = new \Zend\Mail\Message();
        $objEmail->setBody($message);
        $objEmail->setFrom('indrajeet.sarita02@gmail.com', 'From');
        $objEmail->addTo('indrajeet.sarita02@gmail.com', 'To');
        $objEmail->setSubject($subject);
//                $transport = new \Zend\Mail\Transport\Sendmail(/* '-freturn_to_me@example.com' */);
//                $transport->send($mail);
        // Setup SMTP transport using PLAIN authentication over TLS
        $transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions(array(
            'name' => 'smtp.gmail.com',
            'host' => 'smtp.gmail.com',
            'port' => 587, // Notice port change for TLS is 587
            'connection_class' => 'plain',
            'connection_config' => array(
                'username' => '',
                'password' => '',
                'ssl' => 'tls',
            ),
        ));
        $transport->setOptions($options);
        //$transport->send($objEmail);
    }

    public function aboutusAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(1);
        return new ViewModel(array('content' => $content));
    }

    public function termsAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(2);
        return new ViewModel(array('content' => $content));
    }

    public function privacyAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(3);
        return new ViewModel(array('content' => $content));
    }

    public function faqsAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(4);
        return new ViewModel(array('content' => $content));
    }

    public function careersAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(5);
        return new ViewModel(array('content' => $content));
    }

    public function getCityAction() {
        $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $mstCityTable = new TableGateway('mst_city', $this->dbAdapter);
        $cityRows = $mstCityTable->select(array('status' => 1));
        return $cityRows;
    }

}

/*
$compoundIds = array(68,74,112);
$select = $db->select()
   ->from('compounds', array('compounds_id' => 'id')
   ->where('compounds.id in ( ? )', $compoundIds)
   ->join('reaction_compound', 'compounds.id = reaction_compound.compound', array('reaction_compound_number' => 'number'))
   ->join('reactions', 'reactions.id = reaction_compound.reaction', array('reaction_id' => 'id');
 *  */

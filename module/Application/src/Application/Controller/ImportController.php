<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use User\Model\User;
use User\Model\UserTable;
use Zend\Search\Lucene;
use Zend\Search\Lucene\Document;
use Zend\Search\Lucene\Index;
#use Zend\Db\Sql\Sql;
#use Zend\Db\ResultSet\ResultSet;
#use Zend\View\Model\ViewModel;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\Exception\RuntimeException;
use Zend\Console\Request as ConsoleRequest;

class ImportController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function generateIndexAction() {
        $limit = (int) ITEM_PERPAGE;
        $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $searchDataTable = new \Zend\Db\TableGateway\TableGateway('search_data', $this->dbAdapter);
        $sql = "TRUNCATE TABLE search_data";
        $statement = $this->dbAdapter->query($sql);
        $statement->execute();
        $searchIndexLocation = BASE_DIR . '/uploads/search_index';
        $this->_clearDirectory($searchIndexLocation);
        $index = Lucene\Lucene::create($searchIndexLocation);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $uploadPath = BASE_DIR . '/uploads/resume';
        $userCnt = $userTable->getCount();
        $userTable->siteMapXML('', '', 0);
        $numPages = ceil($userCnt['cnt'] / $limit);
        for ($i = 1; $i <= $numPages; $i++) {
            $page = ($i * $limit) - $limit;
            echo $page . "\n";
            $userData = $userTable->getAllUsers($limit, $page);
            foreach ($userData as $row) {
                // id field
                $uid = Document\Field::unIndexed('uid', $row['id']);
                // full name field
                $fullname = Document\Field::Text('name', $row['fname'] . ' ' . $row['lname']);
                // email field    		
                $image = Document\Field::Text('image', $row['image']);
                $email = Document\Field::Text('email', $row['email']);
                $usertype = Document\Field::Text('usertype', $row['usertype']);
                $aboutus = Document\Field::Text('aboutus', $row['aboutus']);
                $otherinfo = Document\Field::Text('otherinfo', $row['otherinfo']);
                $address = Document\Field::Text('address', $row['address']);
                $address2 = Document\Field::Text('address2', $row['address2']);
                $city = Document\Field::Text('city', $row['city']);
                $zip = Document\Field::Text('zip', $row['zip']);
                $phone = Document\Field::Text('phone', $row['phone']);
                $mobile = Document\Field::Text('mobile', $row['mobile']);
                $fax = Document\Field::Text('fax', $row['fax']);
                $email1 = Document\Field::Text('email1', $row['email1']);
                $summary = Document\Field::Text('summary', $row['summary']);
                $websiteurl = Document\Field::Text('websiteurl', $row['websiteurl']);
                // add data from category, skill and court
                $resLawCat = $userTable->getLawyerCategory($row['id']);
                if (isset($resLawCat) && !empty($resLawCat)) {
                    $arrCat = array();
                    foreach ($resLawCat as $val) {
                        $arrCat[] = $val['title'];
                    }
                    $strCat = array_unique($arrCat);
                    $category = Document\Field::Text('category', trim(implode(',', $strCat), ','));
                }
                $resLawCort = $userTable->getLawyerCourt($row['id']);
                if (isset($resLawCort) && !empty($resLawCort)) {
                    $arrCort = array();
                    foreach ($resLawCort as $val) {
                        $arrCort[] = $val['title'];
                    }
                    $strCort = array_unique($arrCort);
                    $court = Document\Field::Text('court', trim(implode(',', $strCort), ','));
                }
                $resLawSkil = $userTable->getLawyerSkill($row['id']);
                if (isset($resLawSkil) && !empty($resLawSkil)) {
                    $arrSkil = array();
                    foreach ($resLawSkil as $val) {
                        $arrSkil[] = $val['title'];
                    }
                    $strSkil = array_unique($arrSkil);
                    $skill = Document\Field::Text('skill', trim(implode(',', $strSkil), ','));
                }

                if (!empty($row['resume']) && file_exists($uploadPath . '/' . $row['resume'])) {
                    if (substr_compare($row['resume'], '.xlsx', strlen($row['resume']) - strlen('.xlsx'), strlen('.xlsx')) === 0) {
                        // index excel sheet
                        $indexDoc = Lucene\Document\Xlsx::loadXlsxFile($uploadPath . '/' . $row['resume']);
                    } else if (substr_compare($row['resume'], '.docx', strlen($row['resume']) - strlen('.docx'), strlen('.docx')) === 0) {
                        // index word doc
                        $indexDoc = Lucene\Document\Docx::loadDocxFile($uploadPath . '/' . $row['resume']);
                    }
                } else {
                    $indexDoc = new Lucene\Document();
                }
                $indexDoc->addField($fullname);
                $indexDoc->addField($image);
                $indexDoc->addField($email);
                $indexDoc->addField($usertype);
                $indexDoc->addField($aboutus);
                $indexDoc->addField($otherinfo);
                $indexDoc->addField($address);
                $indexDoc->addField($address2);
                $indexDoc->addField($city);
                $indexDoc->addField($zip);
                $indexDoc->addField($phone);
                $indexDoc->addField($mobile);
                $indexDoc->addField($fax);
                $indexDoc->addField($email1);
                $indexDoc->addField($summary);
                $indexDoc->addField($websiteurl);
                $indexDoc->addField($category);
                $indexDoc->addField($court);
                $indexDoc->addField($skill);
                $indexDoc->addField($uid);
                $index->addDocument($indexDoc);

                // insert data in search table
                $data['id'] = $row['id'];
                $data['name'] = $row['fname'] . " " . $row['lname'];
                $data['image'] = $row['image'];
                $data['email'] = $row['email'];
                $data['usertype'] = $row['usertype'];
                $data['dob'] = $row['dob'];
                $data['aboutus'] = $row['aboutus'];
                $data['otherinfo'] = $row['otherinfo'];
                $data['resume'] = $row['resume'];
                $data['address'] = $row['address'];
                $data['address2'] = $row['address2'];
                $data['city'] = $row['city'];
                $data['state'] = $row['state'];
                $data['country'] = $row['country'];
                $data['zip'] = $row['zip'];
                $data['phone'] = $row['phone'];
                $data['mobile'] = $row['mobile'];
                $data['fax'] = $row['fax'];
                $data['email1'] = $row['email1'];
                $data['summary'] = $row['summary'];
                $data['websiteurl'] = $row['websiteurl'];
                $data['galleryimage'] = $row['galleryimage'];
                $data['category'] = trim(implode(', ', $strCat), ', ');
                $data['court'] = trim(implode(', ', $strCort), ', ');
                $data['skill'] = trim(implode(', ', $strSkil), ', ');
                $data['isfeatured'] = $row['isfeatured'];
                $data['status'] = $row['status'];
                $data['membershiptype'] = $row['membershiptype'];
                $searchDataTable->insert($data);
                $userTable->siteMapXML($data['id'], $data['name'], 1);
            }
        }
        $index->optimize();
        $index->commit();
        $userTable->siteMapXML('', '', 2);
        unset($index);
        echo "----Done----\n";
    }

    private function _clearDirectory($dirName) {
        if (!file_exists($dirName) || !is_dir($dirName)) {
            return;
        }
        // remove files from temporary directory
        $dir = opendir($dirName);
        while (($file = readdir($dir)) !== false) {
            if (!is_dir($dirName . '/' . $file)) {
                unlink($dirName . '/' . $file); //echo $dirName . '/' . $file.'<br>';
            }
        }
        closedir($dir);
    }

    public function dataAction() {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }

        // Get system service name  from console and check if the user used --verbose or -v flag
        $doname = $request->getParam('doname', false);
        $verbose = $request->getParam('verbose');

        $shell = "ps aux";
        if ($doname) {
            $shell .= " |grep -i $doname ";
        }
        $shell .= " > " . BASE_DIR . "/success.txt";
        //execute...
        system($shell, $val);

        if (!$verbose) {
            echo "Process listed in {BASE_DIR}/success.txt \r\n";
        } else {
            $file = fopen(BASE_DIR . '/success.txt', "r");

            while (!feof($file)) {
                $listprocess = trim(fgets($file));

                echo $listprocess . "\r\n";
            }
            fclose($file);
        }
    }

    public function functionName($param) {
        try {
            $console = Console::getInstance();
        } catch (ConsoleException $e) {
            // Could not get console adapter - most likely we are not running inside a console window.
        }

        $console = $this->getServiceLocator()->get('console');
        if (!$console instanceof Console) {
            throw new RuntimeException('Cannot obtain console adapter. Are we running in a console?');
        }
    }

}

/*
$compoundIds = array(68,74,112);
$select = $db->select()
   ->from('compounds', array('compounds_id' => 'id')
   ->where('compounds.id in ( ? )', $compoundIds)
   ->join('reaction_compound', 'compounds.id = reaction_compound.compound', array('reaction_compound_number' => 'number'))
   ->join('reactions', 'reactions.id = reaction_compound.reaction', array('reaction_id' => 'id');
 *  */

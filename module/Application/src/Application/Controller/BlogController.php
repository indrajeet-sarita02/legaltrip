<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BlogController extends AbstractActionController {

    public function indexAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(16);
        $query = $this->params()->fromQuery('q', '');
        $catId = $this->params()->fromRoute('id');
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $paginator = $blogTable->fetchAll($query, $catId);
        // set the current page to what has been passed in query string, or to 1 if none set
        $page = $this->params()->fromQuery('page', 1);
        $paginator->setCurrentPageNumber((int) $page);
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(ITEM_PERPAGE);
        $blogComment = $blogTable->getComment(0, 5);
        $blogCategory = $blogTable->getCategory();
        return new ViewModel(array('results' => $paginator, 'blogComment' => $blogComment, 'blogCategory' => $blogCategory, 'content' => $content));
    }

    public function blogdetailAction() {
        $id = $this->params()->fromRoute('id');
        if (empty($id)) {
            return $this->redirect()->toRoute('blog');
        }
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(17);
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blog = $blogTable->getBlog($id);
        $blogComment = $blogTable->getComment($id);
        $blogCategory = $blogTable->getCategory();
        return new ViewModel(array('row' => $blog, 'blogComment' => $blogComment, 'blogCategory' => $blogCategory, 'content' => $content));
    }

    public function commentAction() {
        if ($this->request->isPost()) {
            $staus = false;
            $msg = '';
            $post = $this->request->getPost();
            // save comment data
            $name = isset($post->name) ? $post->name : '';
            $email = isset($post->email) ? $post->email : '';
            $website = isset($post->website) ? $post->website : '';
            $content = isset($post->content) ? $post->content : '';
            $blog_id = isset($post->blog_id) ? $post->blog_id : '';
            $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $data = array(
                'author' => $name,
                'email' => $email,
                'url' => $website,
                'content' => $content,
                'blog_id' => $blog_id,
                'created' => time()
            );
            $contactTable = new \Zend\Db\TableGateway\TableGateway('comment', $this->dbAdapter);
            $msg = $contactTable->insert($data);
            if ($msg) {
                $staus = true;
                $msg = 'Your comment has been successfull.';
            }
            echo json_encode(array('status' => $staus, 'msg' => $msg));
            exit;
        }
    }

    public function editblogAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $arrCategory = array();
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blogCategory = $blogTable->getCategory();
        foreach ($blogCategory as $val) {
            $arrCategory[$val->id] = $val->title;
        }
        $form = new \Admin\Form\BlogForm($arrCategory);
        if (!empty($this->params()->fromRoute('id'))) {
            $blog = $blogTable->getBlog($this->params()->fromRoute('id'));
            $form->bind($blog);
        }

        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as indicated below.', 'form' => $form,));
            }
            // Save user
            $blog = new \Admin\Model\Blog();
            $blog->exchangeArray($post);
            $blog->author = $userSession->fname . ' ' . $userSession->lname;
            $blogTable->saveBlog($blog);
            return $this->redirect()->toRoute('user/user', array('action' => 'index'));
        }
        return $model;
    }

    public function newblogAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $arrCategory = array();
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $blogCategory = $blogTable->getCategory();
        foreach ($blogCategory as $val) {
            $arrCategory[$val->id] = $val->title;
        }
        $form = new \Admin\Form\BlogForm($arrCategory);
        $model = new ViewModel(array('form' => $form));
        if ($this->request->isPost()) {
            $post = $this->request->getPost();
            $form->setData($post);
            if (!$form->isValid()) {
                $model = new ViewModel(array('error' => 'There were one or more issues with your submission. Please correct them as 
                    indicated below.', 'form' => $form,));
            }
            $title = $post['title'];
            $checkData = $blogTable->getBlogByTitle($title);
            if (isset($checkData->title) && !empty($checkData->title)) {
                $model = new ViewModel(array('error' => 'Title already Exists.', 'form' => $form));
                return $model;
            } else {
                // Create user
                $blog = new \Admin\Model\Blog();
                $blog->exchangeArray($post);
                $blog->author = $userSession->fname . ' ' . $userSession->lname;
                $blogTable->saveBlog($blog);
                return $this->redirect()->toRoute('user/user', array('action' => 'index'));
            }
        }
        return $model;
    }

    public function deleteAction() {
        $userSession = new \Zend\Session\Container('siteUser');
        $uid = $userSession->id;
        if (empty($uid)) {
            return $this->redirect()->toRoute('user/login');
        }
        $blogTable = $this->getServiceLocator()->get('BlogTable');
        $id = $this->params()->fromRoute('id');
        $post['status'] = '-1';
        $userStaus = $blogTable->updateFields($post, $id);
        if ($userStaus) {
            return $this->redirect()->toRoute('user/user', array('action' => 'index'));
        }
    }

}

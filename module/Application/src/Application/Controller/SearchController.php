<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use User\Model\User;
use User\Model\UserTable;
use Zend\Search\Lucene;
use Zend\Search\Lucene\Document;
use Zend\Search\Lucene\Index;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

class SearchController extends AbstractActionController {

    protected $storage;
    protected $authservice;

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function indexAction() {
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(12);
        $limit = (int) ITEM_PERPAGE;
        // select data for filter nav
        $userTable = $this->getServiceLocator()->get('UserTable');
        // get cache service
        $cache = $this->getServiceLocator()->get('cache');
        $setResults = array();
        $searchResults = array();
        $dataFilters = array();
        // master case category data
        $caseCatData = array();
        // set unique Cache key
        $catKey = md5('mstCaseCategory');
        // get the Cache data
        $catResult = $cache->getItem($catKey, $catSuccess);
        if (!$catSuccess) {
            $mstCaseCatRows = $userTable->getMstCaseCategory();
            foreach ($mstCaseCatRows as $val) {
                $caseCatData[$val['title']] = $val['title'];
            }
            $dataFilters['mstCaseCat'] = $caseCatData;
            $cache->setItem($catKey, $caseCatData);
        } else {
            $dataFilters['mstCaseCat'] = $catResult;
        }

        // master case court data
        $caseCourtData = array();
        // set unique Cache key
        $cortKey = md5('mstCaseCourt');
        // get the Cache data
        $cortResult = $cache->getItem($cortKey, $cortSuccess);
        if (!$cortSuccess) {
            $mstCaseCourtRows = $userTable->getMstCaseCourt();
            foreach ($mstCaseCourtRows as $val) {
                $caseCourtData[$val['title']] = $val['title'];
            }
            $dataFilters['mstCaseCourt'] = $caseCourtData;
            $cache->setItem($cortKey, $caseCourtData);
        } else {
            $dataFilters['mstCaseCourt'] = $cortResult;
        }

        // master case skill data
        $skillData = array();
        // set unique Cache key
        $skilKey = md5('mstSkill');
        // get the Cache data
        $skilResult = $cache->getItem($skilKey, $skilSuccess);
        if (!$skilSuccess) {
            $mstSkillRows = $userTable->getMstSkill();
            foreach ($mstSkillRows as $val) {
                $skillData[$val['title']] = $val['title'];
            }
            $dataFilters['mstSkill'] = $skillData;
            $cache->setItem($skilKey, $skillData);
        } else {
            $dataFilters['mstSkill'] = $skilResult;
        }

        $query = $this->params()->fromQuery('q', '');
        $type = $this->params()->fromQuery('type', '');
        $city = $this->params()->fromQuery('city', '');
        $category = urldecode($this->params()->fromQuery('category', ''));
        $skill = urldecode($this->params()->fromQuery('skill', ''));
        $court = urldecode($this->params()->fromQuery('court', ''));
        $offset = $this->params()->fromQuery('page', 1);
        $searchIndexLocation = BASE_DIR . '/uploads/search_index';
        //$setLimit = $limit * $offset;
        //Lucene\Lucene::setResultSetLimit($setLimit);
        $index = Lucene\Lucene::open($searchIndexLocation);
        $qry = '';
        if (!empty($query)) {
            //$query = str_replace(' ',  ' ', $query);
            //$qry = $query . "{$query} and ($query or $query) " . " *$query* " . " [$query] " . "{$query}";
            if (is_numeric($query)) {
                $qry .= '( phone:' . $query;
                $qry .= ' or mobile:' . $query;
                $qry .= ' or fax:' . $query . " )";
            } else {
                $qry .= 'name:' . $query;
                $qry .= ' or email:' . $query;
                $qry .= ' or aboutus:' . $query;
                $qry .= ' or otherinfo:' . $query;
                $qry .= ' or address:' . $query;
                $qry .= ' or address2:' . $query;
                $qry .= ' or email1:' . $query;
                $qry .= ' or summary:' . $query;
                $qry = "(" . $qry . ' or websiteurl:' . $query . ")";
            }

            if (!empty($type)) {
                $qry .= ' and ( usertype:' . $type . ")";
            }

            if (!empty($city)) {
                $qry .= ' and ( city:' . $city . ' or state:' . $city . ' or address:' . $city . ")";
            }

            if (!empty($category)) {
                $qry .= ' and ( category:' . $category . ")";
            }

            if (!empty($court)) {
                $qry .= ' and ( court:' . $court . ")";
            }

            if (!empty($skill)) {
                $qry .= ' and ( skill:' . $skill . ")";
            }
            $qry = trim($qry, ', ');

            $searchResults = $index->find($qry, 'score', SORT_NUMERIC, SORT_ASC, 'name', SORT_STRING, SORT_ASC);
        } else {
            return $this->redirect()->toUrl(BASE_URL);
        }
        //echo count($searchResults);
        // this is the cool part
        $pageStart = ($limit * ($offset - 1));
        $pageEnd = ($limit * $offset);
        for ($i = $pageStart; $i < $pageEnd; $i++) {
            if (array_key_exists($i, $searchResults)) {
                $setResults[] = $searchResults[$i];
            } else {
                break;
            }
        }
        //end of cool part
        //$pagination->url = $this->url->link('product/search', $url . '&page={page}');
        $url = preg_replace('/&page=[0-9]/', '', $_SERVER['REQUEST_URI']);
        $config['url'] = $url . '&page={page}';
        $config['total'] = count($searchResults);
        $config['page'] = $offset;
        $config['limit'] = $limit;
        $links = $this->paginate($config);

        // prepare search form
        // $form = new \Zend\Form\Form();
        /* $form->add(array(
          'name' => 'query',
          'attributes' => array(
          'type' => 'text',
          'value' => (isset($query) && $query != 'com') ? $query : '',
          'id' => 'query',
          'placeholder' => 'Search String'
          ),
          'options' => array(
          'label' => 'Search String',
          ),
          ));
          $form->add(array(
          'name' => 'submit',
          'attributes' => array(
          'type' => 'submit',
          'value' => 'Search',
          ),
          )); */
        $select = array();
        $select['category'] = $category;
        $select['skill'] = $skill;
        $select['court'] = $court;
        $select['query'] = $query;
        $select['city'] = $city;
        $select['type'] = $type;

        $keywordSession = new \Zend\Session\Container('keyword');
        $keywordSession['query'] = $query;
        $keywordSession['city'] = $city;
        $keywordSession['type'] = $type;

        $viewModel = new ViewModel(array('select' => $select, 'searchResults' => $setResults, 'links' => $links, 'query' => $query, 'dataFilters' => $dataFilters, 'content' => $content));
        return $viewModel;
    }

    private function _clearDirectory($dirName) {
        if (!file_exists($dirName) || !is_dir($dirName)) {
            return;
        }
        // remove files from temporary directory
        $dir = opendir($dirName);
        while (($file = readdir($dir)) !== false) {
            if (!is_dir($dirName . '/' . $file)) {
                unlink($dirName . '/' . $file); //echo $dirName . '/' . $file.'<br>';
            }
        }
        closedir($dir);
    }

    public function generateIndexAction() {
        $limit = (int) ITEM_PERPAGE;
        $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $searchDataTable = new \Zend\Db\TableGateway\TableGateway('search_data', $this->dbAdapter);
        $sql = "TRUNCATE TABLE search_data";
        $statement = $this->dbAdapter->query($sql);
        $statement->execute();
        $siteMap = BASE_DIR . '/sitemap.xml';
        @unlink($siteMap);
        file_put_contents($siteMap, '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.google.com/schemas/sitemap/0.90">', FILE_APPEND);
        $searchIndexLocation = BASE_DIR . '/uploads/search_index';
        $this->_clearDirectory($searchIndexLocation);
        $index = Lucene\Lucene::create($searchIndexLocation);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $uploadPath = BASE_DIR . '/uploads/resume';
        $userCnt = $userTable->getCount();
        $userTable->siteMapXML('', '', 0);
        $numPages = ceil($userCnt['cnt'] / $limit);
        for ($i = 1; $i <= $numPages; $i++) {
            $page = ($i * $limit) - $limit;
            echo $page . "\n";
            $userData = $userTable->getAllUsers($limit, $page);
            foreach ($userData as $row) {
                // id field
                $uid = Document\Field::unIndexed('uid', $row['id']);
                // full name field
                $fullname = Document\Field::Text('name', $row['fname'] . ' ' . $row['lname']);
                // email field    		
                $image = Document\Field::Text('image', $row['image']);
                $email = Document\Field::Text('email', $row['email']);
                $usertype = Document\Field::Text('usertype', $row['usertype']);
                $aboutus = Document\Field::Text('aboutus', $row['aboutus']);
                $otherinfo = Document\Field::Text('otherinfo', $row['otherinfo']);
                $address = Document\Field::Text('address', $row['address']);
                $address2 = Document\Field::Text('address2', $row['address2']);
                $city = Document\Field::Text('city', $row['city']);
                $zip = Document\Field::Text('zip', $row['zip']);
                $phone = Document\Field::Text('phone', $row['phone']);
                $mobile = Document\Field::Text('mobile', $row['mobile']);
                $fax = Document\Field::Text('fax', $row['fax']);
                $email1 = Document\Field::Text('email1', $row['email1']);
                $summary = Document\Field::Text('summary', $row['summary']);
                $websiteurl = Document\Field::Text('websiteurl', $row['websiteurl']);
                // add data from category, skill and court
                $resLawCat = $userTable->getLawyerCategory($row['id']);
                if (isset($resLawCat) && !empty($resLawCat)) {
                    $arrCat = array();
                    foreach ($resLawCat as $val) {
                        $arrCat[] = $val['title'];
                    }
                    $strCat = array_unique($arrCat);
                    $category = Document\Field::Text('category', trim(implode(',', $strCat), ','));
                }
                $resLawCort = $userTable->getLawyerCourt($row['id']);
                if (isset($resLawCort) && !empty($resLawCort)) {
                    $arrCort = array();
                    foreach ($resLawCort as $val) {
                        $arrCort[] = $val['title'];
                    }
                    $strCort = array_unique($arrCort);
                    $court = Document\Field::Text('court', trim(implode(',', $strCort), ','));
                }
                $resLawSkil = $userTable->getLawyerSkill($row['id']);
                if (isset($resLawSkil) && !empty($resLawSkil)) {
                    $arrSkil = array();
                    foreach ($resLawSkil as $val) {
                        $arrSkil[] = $val['title'];
                    }
                    $strSkil = array_unique($arrSkil);
                    $skill = Document\Field::Text('skill', trim(implode(',', $strSkil), ','));
                }

                if (!empty($row['resume']) && file_exists($uploadPath . '/' . $row['resume'])) {
                    if (substr_compare($row['resume'], '.xlsx', strlen($row['resume']) - strlen('.xlsx'), strlen('.xlsx')) === 0) {
                        // index excel sheet
                        $indexDoc = Lucene\Document\Xlsx::loadXlsxFile($uploadPath . '/' . $row['resume']);
                    } else if (substr_compare($row['resume'], '.docx', strlen($row['resume']) - strlen('.docx'), strlen('.docx')) === 0) {
                        // index word doc
                        $indexDoc = Lucene\Document\Docx::loadDocxFile($uploadPath . '/' . $row['resume']);
                    }
                } else {
                    $indexDoc = new Lucene\Document();
                }
                $indexDoc->addField($fullname);
                $indexDoc->addField($image);
                $indexDoc->addField($email);
                $indexDoc->addField($usertype);
                $indexDoc->addField($aboutus);
                $indexDoc->addField($otherinfo);
                $indexDoc->addField($address);
                $indexDoc->addField($address2);
                $indexDoc->addField($city);
                $indexDoc->addField($zip);
                $indexDoc->addField($phone);
                $indexDoc->addField($mobile);
                $indexDoc->addField($fax);
                $indexDoc->addField($email1);
                $indexDoc->addField($summary);
                $indexDoc->addField($websiteurl);
                $indexDoc->addField($category);
                $indexDoc->addField($court);
                $indexDoc->addField($skill);
                $indexDoc->addField($uid);
                $index->addDocument($indexDoc);

                // insert data in search table
                $data['id'] = $row['id'];
                $data['name'] = $row['fname'] . " " . $row['lname'];
                $data['image'] = $row['image'];
                $data['email'] = $row['email'];
                $data['usertype'] = $row['usertype'];
                $data['dob'] = $row['dob'];
                $data['aboutus'] = $row['aboutus'];
                $data['otherinfo'] = $row['otherinfo'];
                $data['resume'] = $row['resume'];
                $data['address'] = $row['address'];
                $data['address2'] = $row['address2'];
                $data['city'] = $row['city'];
                $data['state'] = $row['state'];
                $data['country'] = $row['country'];
                $data['zip'] = $row['zip'];
                $data['phone'] = $row['phone'];
                $data['mobile'] = $row['mobile'];
                $data['fax'] = $row['fax'];
                $data['email1'] = $row['email1'];
                $data['summary'] = $row['summary'];
                $data['websiteurl'] = $row['websiteurl'];
                $data['galleryimage'] = $row['galleryimage'];
                $data['category'] = trim(implode(', ', $strCat), ', ');
                $data['court'] = trim(implode(', ', $strCort), ', ');
                $data['skill'] = trim(implode(', ', $strSkil), ', ');
                $data['isfeatured'] = $row['isfeatured'];
                $data['status'] = $row['status'];
                $data['membershiptype'] = $row['membershiptype'];
                $searchDataTable->insert($data);
                $userTable->siteMapXML($data['id'], $data['name'], 1);
            }
        }
        $index->optimize();
        $index->commit();
        $userTable->siteMapXML('', '', 2);
        unset($index);
        echo "----Done----\n";
    }

    public function cleanUrl($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return trim(strtolower(preg_replace('/-+/', '-', $string)), '-') . ".html"; // Replaces multiple hyphens with single one.
    }

    public function mysqlAction() {
        $adapter = $this->getServiceLocator()->get("Zend\Db\Adapter\Adapter");
        $limit = 3;
        // select data for filter nav
        $userTable = $this->getServiceLocator()->get('UserTable');
        // get cache service
        $cache = $this->getServiceLocator()->get('cache');
        $setResults = array();
        $searchResults = array();
        $dataFilters = array();
        // master case category data
        $caseCatData = array();
        // set unique Cache key
        $catKey = md5('mstCaseCategory');
        // get the Cache data
        $catResult = $cache->getItem($catKey, $catSuccess);
        if (!$catSuccess) {
            $mstCaseCatRows = $userTable->getMstCaseCategory();
            foreach ($mstCaseCatRows as $val) {
                $caseCatData[$val['title']] = $val['title'];
            }
            $dataFilters['mstCaseCat'] = $caseCatData;
            $cache->setItem($catKey, $caseCatData);
        } else {
            $dataFilters['mstCaseCat'] = $catResult;
        }

        // master case court data
        $caseCourtData = array();
        // set unique Cache key
        $cortKey = md5('mstCaseCourt');
        // get the Cache data
        $cortResult = $cache->getItem($cortKey, $cortSuccess);
        if (!$cortSuccess) {
            $mstCaseCourtRows = $userTable->getMstCaseCourt();
            foreach ($mstCaseCourtRows as $val) {
                $caseCourtData[$val['title']] = $val['title'];
            }
            $dataFilters['mstCaseCourt'] = $caseCourtData;
            $cache->setItem($cortKey, $caseCourtData);
        } else {
            $dataFilters['mstCaseCourt'] = $cortResult;
        }

        // master case skill data
        $skillData = array();
        // set unique Cache key
        $skilKey = md5('mstSkill');
        // get the Cache data
        $skilResult = $cache->getItem($skilKey, $skilSuccess);
        if (!$skilSuccess) {
            $mstSkillRows = $userTable->getMstSkill();
            foreach ($mstSkillRows as $val) {
                $skillData[$val['title']] = $val['title'];
            }
            $dataFilters['mstSkill'] = $skillData;
            $cache->setItem($skilKey, $skillData);
        } else {
            $dataFilters['mstSkill'] = $skilResult;
        }

        $query = $this->params()->fromQuery('q', '');
        $type = $this->params()->fromQuery('type', '');
        $city = $this->params()->fromQuery('city', '');
        $category = urldecode($this->params()->fromQuery('category', ''));
        $skill = urldecode($this->params()->fromQuery('skill', ''));
        $court = urldecode($this->params()->fromQuery('court', ''));
        $offset = $this->params()->fromQuery('page', 1);

        $qry = '';
        if (!empty($query)) {
            $query = addslashes(strtolower($query));
            if (is_numeric($query)) {
                $qry .= "(phone=" . $query . " or mobile=" . $query . ") ";
            } else {
                $qry .= "LOWER(name) like '%" . $query . "%'";
                $qry .= " or  LOWER(email) like '%" . $query . "%'";
                $qry .= " or  LOWER(aboutus) like '%" . $query . "%'";
                $qry .= " or  LOWER(otherinfo) like '%" . $query . "%'";
                $qry .= " or  LOWER(address) like '%" . $query . "%'";
                $qry .= " or  LOWER(address2) like '%" . $query . "%'";
                $qry .= " or  LOWER(fax) like '%" . $query . "%'";
                $qry .= " or  LOWER(email1) like '%" . $query . "%'";
                $qry .= " or  LOWER(summary) like '%" . $query . "%'";
                $qry = "(" . $qry . " or  LOWER(websiteurl) like '%" . $query . "%')";
            }

            if (!empty($type)) {
                $qry .= " and LOWER(usertype) like '%" . $type . "%'";
            }

            if (!empty($city)) {
                $qry .= " and ( LOWER(city) like '%" . $city . "%' or  LOWER(state) like '%" . $city . "%')";
            }

            if (!empty($category)) {
                $qry .= " and LOWER(category) like '%" . $category . "%'";
            }

            if (!empty($court)) {
                $qry .= " and LOWER(court) like '%" . $court . "%'";
            }

            if (!empty($skill)) {
                $qry .= " and LOWER(skill) like '%" . $skill . "%'";
            }

            $sqlCnt = "SELECT * FROM search_data WHERE $qry";
            $stmtCnt = $adapter->createStatement($sqlCnt);
            $stmtCnt->prepare();
            $resultCnt = $stmtCnt->execute();
            $searchResultsCnt = new ResultSet;
            $searchResultsCnt->initialize($resultCnt);
            if ($offset == 1) {
                $start = 0;
            } else {
                $start = ($offset - 1) * $limit;
            }

            $qry .= "order by name limit $start, $limit";
            $sql = "SELECT * FROM search_data WHERE $qry";
            $stmt = $adapter->createStatement($sql);
            $stmt->prepare();
            $result = $stmt->execute();
            $searchResults = new ResultSet;
            $searchResults->initialize($result);
        } else {
            return $this->redirect()->toUrl(BASE_URL);
        }


        $url = preg_replace('/&page=[0-9]/', '', $_SERVER['REQUEST_URI']);
        $config['url'] = $url . '&page={page}';
        $config['total'] = count($searchResultsCnt);
        $config['page'] = $offset;
        $config['limit'] = $limit;
        $links = $this->paginate($config);

        $select = array();
        $select['category'] = $category;
        $select['skill'] = $skill;
        $select['court'] = $court;
        $select['query'] = $query;
        $select['city'] = $city;
        $select['type'] = $type;

        $keywordSession = new \Zend\Session\Container('keyword');
        $keywordSession['query'] = $query;
        $keywordSession['city'] = $city;
        $keywordSession['type'] = $type;

        $viewModel = new ViewModel(array('select' => $select, 'searchResults' => $searchResults, 'links' => $links, 'query' => $query, 'dataFilters' => $dataFilters));
        return $viewModel;
    }

    public function detailAction() {
        $id = $this->params()->fromRoute('id');
        if (empty($id)) {
            return $this->redirect()->toUrl(BASE_URL);
        }
        $id = current(explode("_", $id));
        $contentTable = $this->getServiceLocator()->get('ContentTable');
        $content = $contentTable->getContent(13);
        $userTable = $this->getServiceLocator()->get('UserTable');
        $userData = $userTable->searchData(array('id' => $id));
        $address = $userData->city . " " . $userData->state . " " . $userData->country . " " . $userData->zip; //$userData->address . " " . 
        $userLatLng = $userTable->getLatLng($address);
        $userCityData = $userTable->fetchData(array('city' => $userData->city, 'usertype' => $userData->usertype, "id !={$userData->id}"), 5);
        $model = new ViewModel(array('userData' => $userData, 'userCityData' => $userCityData, 'userLatLng' => $userLatLng, 'content' => $content));
        return $model;
    }

    public function paginate($params = array()) {
        $total = isset($params['total']) ? $params['total'] : 0;
        $params['page'] = isset($params['page']) ? $params['page'] : 1;
        $params['limit'] = isset($params['limit']) ? $params['limit'] : 20;
        $params['num_links'] = isset($params['num_links']) ? $params['num_links'] : 8;
        $params['url'] = isset($params['url']) ? $params['url'] : '';
        $params['text_first'] = isset($params['text_first']) ? $params['text_first'] : '|&lt;';
        $params['text_last'] = isset($params['text_last']) ? $params['text_last'] : '&gt;|';
        $params['text_next'] = isset($params['text_next']) ? $params['text_next'] : '&gt;';
        $params['text_prev'] = isset($params['text_prev']) ? $params['text_prev'] : '&lt;';

        if ($params['page'] < 1) {
            $page = 1;
        } else {
            $page = $params['page'];
        }

        if (!(int) $params['limit']) {
            $limit = 10;
        } else {
            $limit = $params['limit'];
        }

        $num_links = $params['num_links'];
        $num_pages = ceil($total / $limit);

        $params['url'] = str_replace('%7Bpage%7D', '{page}', $params['url']);

        $output = '<ul class="pagination">';

        if ($page > 1) {
            $output .= '<li><a href="' . str_replace('{page}', 1, $params['url']) . '">' . $params['text_first'] . '</a></li>';
            $output .= '<li><a href="' . str_replace('{page}', $page - 1, $params['url']) . '">' . $params['text_prev'] . '</a></li>';
        }

        if ($num_pages > 1) {
            if ($num_pages <= $num_links) {
                $start = 1;
                $end = $num_pages;
            } else {
                $start = $page - floor($num_links / 2);
                $end = $page + floor($num_links / 2);

                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }

                if ($end > $num_pages) {
                    $start -= ($end - $num_pages);
                    $end = $num_pages;
                }
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($page == $i) {
                    $output .= '<li class="active"><span>' . $i . '</span></li>';
                } else {
                    $output .= '<li><a href="' . str_replace('{page}', $i, $params['url']) . '">' . $i . '</a></li>';
                }
            }
        }

        if ($page < $num_pages) {
            $output .= '<li><a href="' . str_replace('{page}', $page + 1, $params['url']) . '">' . $params['text_next'] . '</a></li>';
            $output .= '<li><a href="' . str_replace('{page}', $num_pages, $params['url']) . '">' . $params['text_last'] . '</a></li>';
        }

        $output .= '</ul>';

        if ($num_pages > 1) {
            return $output;
        } else {
            return '';
        }
    }

    public function autoSuggestCityAction() {
        $term = $this->params()->fromQuery('term');
        $this->adapter = $this->getServiceLocator()->get("Zend\Db\Adapter\Adapter");
        $sql = "SELECT * FROM mst_city WHERE LOWER(name) like LOWER('%$term%')";
        $stmt = $this->adapter->createStatement($sql);
        $stmt->prepare();
        $result = $stmt->execute();
        $searchResults = new ResultSet;
        $searchResults->initialize($result);
        $arrCity = array();
        foreach ($searchResults as $val) {
            $arrCity[$val['id']] = $val['name'];
        }
        echo json_encode($arrCity);
        exit;
    }

    public function clearstatcacheAction() {
        $dirPath = BASE_DIR . "/../data/cache/";
        $this->deleteDir($dirPath);
        exit();
    }

    public static function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        //rmdir($dirPath);
    }

}

/*
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;

class StateTable extends AbstractTableGateway{
      
    protected $table = 'states';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function getStateList() {
        $sql = new Sql($this->getAdapter());
        $select = $sql->select()->from(array('state' => $this->table));
        
        // $this->getAdapter()->getDriver()->getConnection()->beginTransaction();
        // $this->getAdapter()->getDriver()->getConnection()->commit();
        // $this->getAdapter()->getDriver()->getConnection()->rollback();

        //$select->columns(array('state_name'));
        //$select->where(array('state.state_id' => '2'));

        //$select->where->NEST->like('state.state_name', 'a%')->OR->like('state.state_name', 'b%');
        //$select->where->OR->NEST->like('state.state_name', 'c%')->OR->like('state.state_name', 'd%');

        //$select->where->nest()->like('state.state_name', 'a%')->OR->like('state.state_name', 'b%');
        //$select->where->AND->nest()->like('state.state_name', 'c%')->OR->like('state.state_name', 'd%');

        //$select->where->equalTo('state.state_name', 'Saint George');
        //$select->where->greaterThan('state.state_id', 5);
        //$select->where->lessThan('state.state_id', 10);
        //$select->where->between('state.state_id', 5, 10);
        //$select->where->greaterThanOrEqualTo('state.state_id', 10);
        //$select->where->isNull('state.state_name');
        //$select->where->isNotNull('state.state_name');
        //$select->where->in('state.state_id', array(1,2,3,4,5));
        //$select->where->notEqualTo('state.state_id', 10);
        //$select->where->lessThanOrEqualTo('state.state_id', 10);
        //$select->limit(5); // always takes an integer/numeric
        //$select->offset(10); // similarly takes an integer/numeric

        $select->join(array('country' => 'countries'), 'country.country_id = state.country_id', array('country_name'), $select::JOIN_INNER);

        $select->order(array('country.country_name ASC'));
        
        //$select->columns(array('stateList' => new \Zend\Db\Sql\Expression("GROUP_CONCAT(state.state_name ORDER BY state.state_name ASC SEPARATOR ', ')")));

        //$select->group(array('state.country_id'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        
        //print($statement->getSql());

        $states = $this->resultSetPrototype->initialize($statement->execute())->toArray();
        return $states;
    }
}
 *  */
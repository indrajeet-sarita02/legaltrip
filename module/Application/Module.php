<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\View\Helper;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Model\ContentTable;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface,
    Zend\ModuleManager\Feature\ConfigProviderInterface,
    Zend\ModuleManager\Feature\ConsoleUsageProviderInterface,
    Zend\Console\Adapter\AdapterInterface as Console;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ConsoleUsageProviderInterface {

    /**
     * This method is defined in ConsoleUsageProviderInterface
     */
    public function getConsoleUsage(Console $console) {
        return array(
            // Describe available commands
            'generateIndex' => 'Generate index for lucene search',
            'get happen [--verbose|-v] <doname>' => 'Get Process already happen',
            // Describe expected parameters
            array('doname', 'Process Name'),
            array('--verbose|-v', '(optional) turn on verbose mode'),
            'delete user <userEmail>' => 'Delete user with email <userEmail>',
            'disable user <userEmail>' => 'Disable user with email <userEmail>',
            'list [all|disabled] users' => 'Show a list of users',
            'find user [--email=] [--name=]' => 'Attempt to find a user by email or name',
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'Params' => function (ServiceLocatorInterface $helpers) {
                    $services = $helpers->getServiceLocator();
                    $app = $services->get('Application');
                    return new Helper\Params($app->getRequest(), $app->getMvcEvent());
                },
                'CityList' => function(ServiceLocatorInterface $helpers) {
                    // Get the shared service manager instance
                    $dbAdapter = $helpers->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                    // get cache service
                    $cache = $helpers->getServiceLocator()->get('cache');
                    return new Helper\CityList($dbAdapter, $cache);
                },
            ),
        );
    }

    public function onBootstrap(MvcEvent $e) {
        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
